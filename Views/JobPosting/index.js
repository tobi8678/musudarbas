import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import "../../inc/fonts/style.css";
import Location from "../../inc/icons/navigation.svg";
import Calendar from "../../inc/icons/calendar.svg";
import Button from "../../Components/Button";

const JobPostingWrapper = styled.div`
  display: block;
  border-radius: 40px 0px 40px 40px;
  box-shadow: 0px 0px 110px -74px rgba(0, 0, 0, 0.75);
`;
const JobPostingContent = styled.div`
  position: relative;
  display: flex;
  padding: 0 100px;
  align-items: center;
`;
const JobPostingFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 2%;
  background-color: #f7f7f7;
  border-radius: 0px 0px 40px 40px;
  padding-top: 20px;
  padding-bottom: 20px;
`;
const JobType = styled.div`
  position: absolute;
  right: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 22px;
  background-color: #f7f7f7;
  border-radius: 0px 0px 0px 40px;
  height: 52px;
  width: auto;
  padding: 0 30px;
  p {
    color: #5d5d5d;
    font-family: "Montserrat";
  }
`;
const CompaniesLogo = styled.div`
  img {
    height: 250px;
    width: auto;
  }
`;
const CompaniesExpert = styled.div`
  padding: 0 8%;
  text-align: center;
  h1 {
    font-size: 40px;
    font-family: "Montserrat", Extra-Bold;
    color: #5d5d5d;
  }
  p {
    font-size: 14px;
    font-family: "Montserrat";
    color: #5d5d5d;
  }
`;
const JobLocation = styled.div`
  display: flex;
  align-items: center;
  p {
    font-family: "Montserrat";
    color: #5d5d5d;
    font-size: 29px;
    margin-left: 20px;
  }
`;
const MoreInformation = styled.div``;
const PostedTime = styled.div`
  display: flex;
  align-items: center;
  p {
    font-family: "Montserrat";
    color: #5d5d5d;
    font-size: 29px;
    margin-left: 20px;
  }
`;
const JobPosting = ({
  jobType,
  companiesLogo,
  companyName,
  descriptionExpert,
  location,
  postedDate
}) => {
  return (
    <JobPostingWrapper>
      <JobType>
        <p>{jobType}</p>
      </JobType>
      <JobPostingContent>
        <CompaniesLogo>
          <img src={companiesLogo}></img>
        </CompaniesLogo>
        <CompaniesExpert>
          <h1>{companyName}</h1>
          <p>{descriptionExpert}</p>
        </CompaniesExpert>
      </JobPostingContent>
      <JobPostingFooter>
        <JobLocation>
          <img src={Location} />
          <p>{location}</p>
        </JobLocation>
        <Button
          backgroundColor="#A0C334"
          color="#FFFFFF"
          text="Daugiau Informacijos"
        />
        <PostedTime>
          <img src={Calendar} />
          <p>Prieš {postedDate} dienas</p>
        </PostedTime>
      </JobPostingFooter>
    </JobPostingWrapper>
  );
};

JobPosting.propTypes = {
  jobType: PropTypes.string,
  companiesLogo: PropTypes.string,
  companyName: PropTypes.string,
  descriptionExpert: PropTypes.string,
  location: PropTypes.string,
  postedDate: PropTypes.number
};

export default JobPosting;
