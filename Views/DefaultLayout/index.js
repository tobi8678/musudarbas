import React from "react";
import DefaultNavigation from "../../Components/DefaultNavigation";
import styled from "styled-components";
const DefaultLayoutWrapper = styled.div`
  background: #a0c334;
  height: 100vh;
`;
const DefaultLayout = () => {
  return (
    <DefaultLayoutWrapper>
      <DefaultNavigation />
    </DefaultLayoutWrapper>
  );
};

export default DefaultLayout;
