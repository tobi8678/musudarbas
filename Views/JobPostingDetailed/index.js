import React from "react";
import PropTypes from "prop-types";
import Button from "../../Components/Button";
import styled from "styled-components";

import Calendar from "../../inc/icons/calendar.svg";
import Location from "../../inc/icons/navigation.svg";
import LeftArrow from "../../inc/icons/left-arrow.svg";
import Logo from "../../inc/icons/musudarbas3.png";

import "../../inc/fonts/style.css";
const JobPostingDetailedWrapper = styled.div`
  display: block;
  border-radius: 40px 40px 0px 0px;
  box-shadow: 0px 0px 49px -33px rgba(0, 0, 0, 0.75);
`;
const JobPostingHeader = styled.div`
  position: relative;
  height: 320px;
  padding: 0 2%;
  border-radius: 40px 40px 0px 0px;
  box-shadow: 0px 3px 49px -33px rgba(0, 0, 0, 0.75);
  .jobposting_logo {
    height: 300px;
    width: auto;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

const BackButton = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  p {
    margin-left: 20px;
    font-size: 34px;
  }
`;

const JobPopstingContent = styled.div`
  display: flex;
`;

const JobPostingRequirements = styled.div``;

const JobInformation = styled.div`
  width: 400px;
  height: auto;
  background-color: #f7f7f7;
  border-radius: 0px 0px 40px 0px;
  box-shadow: 0px 0px 49px -33px rgba(0, 0, 0, 0.75);
  padding: 3%;
  h2 {
    margin: 0;
    text-align: center;
  }
  ul {
    list-style: none;
    li {
      font-size: 20px;
    }
  }
`;
const JobRequirements = styled.div`
  padding: 3%;
  margin-top: 40px;
  width: 400px;
  height: auto;
  background-color: #f7f7f7;
  border-radius: 0px 40px 40px 0px;
  box-shadow: 0px 0px 49px -33px rgba(0, 0, 0, 0.75);
  h2 {
    margin: 0;
    text-align: center;
  }
  li {
    font-size: 20px;
  }
`;
const JobPostingDescription = styled.div`
  padding: 0 8%;
  text-align: center;
  h1 {
    font-size: 40px;
    font-family: "Montserrat", Extra-Bold;
    color: #5d5d5d;
  }
  p {
    font-size: 18px;
    font-family: "Montserrat";
    color: #5d5d5d;
  }
`;

const JobPostingFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 2%;
  background-color: #f7f7f7;
  border-radius: 0px 0px 40px 40px;
  padding-top: 20px;
  padding-bottom: 20px;
  margin-top: 40px;
`;

const JobLocation = styled.div`
  display: flex;
  align-items: center;
  p {
    font-family: "Montserrat";
    color: #5d5d5d;
    font-size: 29px;
    margin-left: 20px;
  }
`;

const PostedTime = styled.div`
  display: flex;
  align-items: center;
  p {
    font-family: "Montserrat";
    color: #5d5d5d;
    font-size: 29px;
    margin-left: 20px;
  }
`;
const JobPostingDetailed = ({
  location,
  postedDate,
  logo,
  companyName,
  description,
  workTime,
  salary,
  workType,
  deadline,
  requirements,
  goBackFunction
}) => {
  return (
    <JobPostingDetailedWrapper>
      <JobPostingHeader>
        <BackButton onClick={goBackFunction}>
          <img src={LeftArrow} />
          <p>GRĮŽTI ATGAL</p>
        </BackButton>
        <img className="jobposting_logo" src={logo ? logo : Logo} />
      </JobPostingHeader>
      <JobPopstingContent>
        <JobPostingRequirements>
          <JobInformation>
            <h2>Informacija apie skelbimą</h2>
            <ul>
              <li>Darbo laikas: {workTime}</li>
              <li>Darbo rūšis: {workType} </li>
              <li>Atlyginimas: {salary} €</li>
              <li style={{ textAlign: "center", marginTop: "10px" }}>
                Paraiškų teikimo terminas: <br /> {deadline}{" "}
              </li>
            </ul>
          </JobInformation>
          <JobRequirements>
            <h2>Darbo pozicijos reikalavimai</h2>
            <ul>
              {requirements.map(requirement => (
                <li>{requirement}</li>
              ))}
            </ul>
          </JobRequirements>
        </JobPostingRequirements>
        <JobPostingDescription>
          <h1>{companyName}</h1>
          <p>{description}</p>
        </JobPostingDescription>
      </JobPopstingContent>
      <JobPostingFooter>
        <JobLocation>
          <img src={Location} />
          <p>{location}</p>
        </JobLocation>
        <Button
          backgroundColor="#A0C334"
          color="#FFFFFF"
          text="Aplekuoti dėl darbo pozicijos"
        />
        <PostedTime>
          <img src={Calendar} />
          <p>Prieš {postedDate} dienas</p>
        </PostedTime>
      </JobPostingFooter>
    </JobPostingDetailedWrapper>
  );
};

JobPostingDetailed.propTypes = {
  companyName: PropTypes.string.isRequired,
  deadline: PropTypes.string,
  description: PropTypes.string,
  goBackFunction: PropTypes.func,
  location: PropTypes.string,
  logo: PropTypes.string,
  postedDate: PropTypes.number,
  requirements: PropTypes.arrayOf(PropTypes.string),
  salary: PropTypes.number,
  workTime: PropTypes.string,
  workType: PropTypes.string
};

export default JobPostingDetailed;
