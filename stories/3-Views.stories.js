import React from "react";
import { action } from "@storybook/addon-actions";
import { Button } from "@storybook/react/demo";
import LoginForm from "../Views/LoginAndRegister";
import JobPosting from "../Views/JobPosting";
import JobPostingDetailed from "../Views/JobPostingDetailed";
import DefaultLayout from "../Views/DefaultLayout";

import Logo from "../inc/icons/musudarbas3.png";

export default {
  title: "Views"
};

export const LoginFormDummy = () => <LoginForm></LoginForm>;

export const JobPostingDummy = () => (
  <JobPosting
    companiesLogo={Logo}
    companyName="Testing"
    descriptionExpert="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab, tempora molestiae cupiditate quidem temporibus veniam quibusdam, at perferendis corporis, et quam ipsa totam voluptatum libero repellat reprehenderit cumque. Soluta, consectetur!
  Amet accusamus voluptate perspiciatis, porro nemo totam accusantium quisquam corrupti omnis explicabo dolorem exercitationem! Accusamus sed, modi culpa deleniti quasi nihil architecto maxime porro non ab veritatis maiores corporis iure.
  Harum, in doloribus? Aliquid sed odio quam autem suscipit necessitatibus sunt illum rerum tempore voluptate magni itaque cum cupiditate neque maiores incidunt obcaecati laborum dolore, vel atque deleniti. Quo, corporis?"
    jobType="Pilnas Etatas"
    location="Vilnius"
    postedDate={3}
  ></JobPosting>
);

const JobRequirements = ["test", "test", "test"];

export const JobPostingDetailedDummy = () => (
  <JobPostingDetailed
    salary={1500}
    workType="IT"
    workTime="Pilnas Etatas"
    deadline="10-10-2020"
    postedDate={3}
    location="Vilnius"
    companyName="Testing"
    requirements={JobRequirements}
    description="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab, tempora molestiae cupiditate quidem temporibus veniam quibusdam, at perferendis corporis, et quam ipsa totam voluptatum libero repellat reprehenderit cumque. Soluta, consectetur!
  Amet accusamus voluptate perspiciatis, porro nemo totam accusantium quisquam corrupti omnis explicabo dolorem exercitationem! Accusamus sed, modi culpa deleniti quasi nihil architecto maxime porro non ab veritatis maiores corporis iure.
  Harum, in doloribus? Aliquid sed odio quam autem suscipit necessitatibus sunt illum rerum tempore voluptate magni itaque cum cupiditate neque maiores incidunt obcaecati laborum dolore, vel atque deleniti. Quo, corporis?"
  />
);

export const DefaultLayoutDummy = () => <DefaultLayout></DefaultLayout>;
