import React from "react";
import InputWithType from "../Components/InputWithType";
import ButtonDummy from "../Components/Button";
import CheckBox from "../Components/CheckBox";
import avatarIcon from "../inc/icons/avatar.png";
import Filtering from "../Components/Filtering";

export default {
  title: "Components"
};


export const CheckBoxDummy = () => (
  <>
    <CheckBox />
  </>
);
export const ButtonDummyTest = () => (
  <div>
    <ButtonDummy text="Test" />
  </div>
);
export const InputAndType = () => (
  <div>
    <InputWithType
      icon={avatarIcon}
      placeholder="Test" 
      name="name"
      type="text" 
    />
  </div>
);

export const Filter = () => {
  return(
    <div>
      <Filtering title="Įvesk "/>
    </div>
  );
}; 
