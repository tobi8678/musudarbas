import React from "react";
import styled from "styled-components";
import "../../inc/fonts/style.css";

const Buttons = ({ text, onClick, backgroundColor, color }) => {
  return (
    <div className="Button_Wrapper">
      <ButtonComponent
        color={color}
        backgroundColor={backgroundColor}
        onClick={onClick}
      >
        {text}
      </ButtonComponent>
    </div>
  );
};
const ButtonComponent = styled.button`
  color: ${props => (props.color ? props.color : "#5d5d5d")};
  background: ${props =>
    props.backgroundColor ? props.backgroundColor : "white"};
  border-radius: 15px;
  border: none;
  font-weight: bold;
  font-size: 24px;
  text-transform: uppercase;
  padding: 20px 35px;
  cursor: pointer;
  border: 1px transparent solid;
  box-shadow: 0px 0px 110px -74px rgba(0, 0, 0, 0.75);
  font-family: "Montserrat";
`;
export default Buttons;
