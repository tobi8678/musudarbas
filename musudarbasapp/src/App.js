import React, { useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "../src/Views/LoginAndRegister";
import styled from "styled-components";
import Navigation from "../src/Views/Navigation";
import Dashboard from "../src/Views/Dashboard";
import AllJobPosts from "../src/Views/AllJobPosts";
import SingleJobPost from "../src/Views/JobPostingDetailed";
import Apply from "../src/Views/Apply";
import { Helmet } from "react-helmet";
import Modal from "../src/Views/Dialog";
const AppWrapper = styled.div`
  background: #a0c334;
  position: relative;
  min-height: 100vh;
`;
const HeadLine = styled.header`
  text-align: center;
  h1 {
    color: white;
    font-weight: 100;
    span {
      font-weight: bold;
    }
  }
`;
function App() {
  const [display, setDisplay] = useState(false);

  const onClick = () => {
    setDisplay(!display);
  };
  return (
    <BrowserRouter basename={"/"}>
      <Helmet>
        <meta
          name="description"
          content="SURASK DARBĄ GREIČIAU IR EFEKTYVIAU! Darbo paieška - paprasta ir efektyvi.Žmogiškųjų išteklių valdymo sistema, kuri padės surasti patį geriausią kandidatą jūsų ieškomai pozicijai."
        />
        <title>Musu Darbas</title>
        <link rel="canonical" href="http://musudarbas.lt" />
      </Helmet>
      {display ? <Login clicked={onClick} /> : <></>}
      <AppWrapper
        onClick={() => {
          if (display) {
            setDisplay(!display);
          }
        }}
        className={display ? "App popup" : "App"}
      >
        <header>
          <Navigation clicked={onClick} />
        </header>

        <Switch>
          <Route component={Dashboard} path="/panele" />
          <Route path="/prisijungimas" component={Login} />
          <Route exact path="/skelbimas/:id">
            <HeadLine>
              <h1>
                SURASK DARBĄ <span>GREIČIAU</span> IR <span>EFEKTYVIAU</span>
              </h1>
            </HeadLine>
            <SingleJobPost />
          </Route>
          <Route path="/skelbimas/:id/aplikavimas">
            <HeadLine>
              <h1>
                APLIKUOK DARBUI <span>GREIČIAU</span> IR <span>EFEKTYVIAU</span>
              </h1>
            </HeadLine>
            <Apply />
          </Route>
          <Route exact path="/" component={AllJobPosts} />
        </Switch>
      </AppWrapper>
    </BrowserRouter>
  );
}

export default App;
