import React, { useEffect, useState } from "react";
import Axios from "axios";

import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEnvelope,
  faPhone,
  faFileAlt
} from "@fortawesome/free-solid-svg-icons";
import avatar from "../../inc/icons/avatar.svg";
import Button from "../../Components/Button";
import Helmet from "react-helmet";
const Contacts = styled.div`
  display: flex;
  justify-content: center;
  font-size: 30px;
  svg {
    height: 30px;
    width: 30px;
  }
  p {
    margin-left: 20px !important;
  }
  .AlignContacts {
    display: flex;
    margin-top: 20px;
    justify-content: center;
  }
`;
const Fails = styled.div`
  .file_wrap {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  a {
    margin-left: 20px;
    font-size: 30px;
  }
  svg {
    height: 35px !important;
    width: 35px !important;
    color: red;
  }
`;
const ButtonStyle = styled.div`
  .Button_Wrapper {
    display: flex;
    justify-content: center;
    margin-top: 5%;
  }
`;
const SingleApplication = ({ match }) => {
  let Profile = JSON.parse(sessionStorage.getItem("user"));
  let token = JSON.parse(sessionStorage.getItem("token"));
  const [fetch, setFetch] = useState(false);
  const [user, setUser] = useState(false);

  console.log(fetch);

  const fetchData = async () => {
    await Axios.get(
      "http://185.34.52.143:1337/applications?id=" + match.params.uid,
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    ).then(async res => {
      setFetch(res.data[0]);
      await Axios.get(
        "http://185.34.52.143:1337/usersdetails/" +
          res.data[0].user.usersdetail,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      ).then(r => setUser(r.data));
    });
  };

  useEffect(() => {
    fetchData();
  }, []);
  let icon = "";
  if (user.image === undefined) {
    icon = avatar;
  } else {
    icon = user.image.base64;
  }

  if (fetch !== undefined) {
    if (fetch.status === "neperskaityta") {
      Axios.put(
        "http://185.34.52.143:1337/applications/" + fetch.id,
        { status: "Perskaitytas" },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      );
    }
  }
  return (
    <div>
      {!fetch ? (
        <h1>Si anketa neagzistuoja</h1>
      ) : (
        <div>
          <Helmet>
            <title>Aplikacija: {fetch.user.username} </title>
            <meta
              name="description"
              content="Vartotojo aplikacija, čia galima rasti žmogaus kontaktus, bei jo CV. Taip pat informacija kurią jis prisegė prie savo anketos."
            />
          </Helmet>
          <h1
            style={{
              textAlign: "center",
              fontWeight: "lighter",
              textTransform: "uppercase"
            }}
          >
            {fetch.user.username}
          </h1>

          <img
            style={{
              height: "250px",
              width: "250px",
              margin: "0 auto",
              display: "block",
              borderRadius: "50%"
            }}
            src={icon}
          />

          <Contacts>
            <div>
              <div className="AlignContacts">
                <FontAwesomeIcon icon={faEnvelope} />
                <p>{fetch.email}</p>
              </div>
              <div className="AlignContacts">
                <FontAwesomeIcon icon={faPhone} />
                <p>{fetch.phone}</p>
              </div>
            </div>
          </Contacts>
          <h1 style={{ textAlign: "center" }}>Failai (CV):</h1>
          <Fails>
            {fetch.cvs.map(cv => (
              <div className="file_wrap">
                <FontAwesomeIcon icon={faFileAlt} />
                <a download={cv.name} href={cv.file}>
                  {cv.name}
                </a>
              </div>
            ))}
          </Fails>
          <p style={{ textAlign: "center", fontSize: "20px", marginTop: "5%" }}>
            {fetch.moreinformation}
          </p>
          <ButtonStyle>
            {" "}
            <Button
              backgroundColor="#A0C334"
              color="white"
              text="< Atgal"
              onClick={() => window.history.back()}
            />
          </ButtonStyle>
        </div>
      )}
    </div>
  );
};

export default SingleApplication;
