import React, { useState, useEffect } from "react";
import axios from "axios";
import Chart from "react-apexcharts";
import folder from "../../inc/icons/folder.svg";
import { useHistory } from "react-router-dom";
import Helmet from "react-helmet";

import styled from "styled-components";
import JobPost from "../JobPost";
const Table = styled.table`
  width: 100%;
  text-align: left;
  border-collapse: collapse;
  font-size: 18px;
  td,
  th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }

  tr:nth-child(even) {
    background-color: #94b42a;
    color: white;
  }
  img {
    cursor: pointer;
  }
`;

const MyPosts = () => {
  const [jobsposts, setJobposts] = useState(false);

  let history = useHistory();
  let jobtypes = [];
  let views = [];
  if (!jobsposts) {
  } else {
    for (let i = 0; i < jobsposts.length; i++) {
      jobtypes.push(jobsposts[i].jobposition);
      views.push(jobsposts[i].views.length);
    }
  }
  console.log(jobsposts);

  let Profile = JSON.parse(sessionStorage.getItem("user"));
  let token = JSON.parse(sessionStorage.getItem("token"));

  const fetchData = async () => {
    const myjobposts = await axios(
      "http://185.34.52.143:1337/jobposts?user.id=" +
        Profile.id +
        "&_sort=created_at:DESC",
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    );

    setJobposts(myjobposts.data);
  };

  useEffect(() => {
    fetchData();
  }, []);
  console.log(jobsposts);

  const chart = {
    options: {
      colors: ["#A0C334"],
      chart: {
        id: "basic-bar"
      },
      xaxis: {
        categories: jobtypes
      }
    },
    series: [
      {
        name: "Perziuros",
        data: views
      }
    ]
  };

  return (
    <div>
      {!jobsposts ? (
        <></>
      ) : (
        <>
          <Helmet>
            <title>Mano Skelbimai</title>
            <meta
              name="description"
              content="Mano darbo skelbimų statistika bei jų statusas. Taip pat raskite kiek žmonių aplikavo jūsų darbo skelbimui."
            />
          </Helmet>
          <h1>Statistika</h1>
          <Chart
            options={chart.options}
            series={chart.series}
            type="bar"
            width="100%"
            height="300px"
          />
          <hr />
          <h1>Aplikaciju apzvalga</h1>
          <Table>
            <tr>
              <th>ID</th>
              <th>Kompanija</th>
              <th>Ieskoma pozicija</th>
              <th>Darbo sritis</th>
              <th>Statusas</th>
              <th>Aplikacijos</th>
              <th>Perziureti</th>
            </tr>
            {jobsposts.map((job, index) => (
              <tr>
                <td style={{ textAlign: "center" }}>{index}</td>
                <td>{job.name}</td>
                <td
                  style={{ textDecoration: "underline", cursor: "pointer" }}
                  onClick={() => history.push("/skelbimas/" + job.id)}
                >
                  {job.jobposition}
                </td>
                <td>{job.jobtype}</td>
                <td
                  style={{
                    color: !job.confirmed ? "gold" : "#013220",
                    textAlign: "center",
                    fontWeight: "bold"
                  }}
                >
                  {job.confirmed ? "PATVIRTINTA" : "LAUKIAMA"}
                </td>
                <td style={{ textAlign: "center" }}>
                  ( {job.applications.length} )
                </td>
                <td style={{ display: "flex", justifyContent: "center" }}>
                  <img
                    style={{ height: "28px", width: "28px" }}
                    src={folder}
                    onClick={() =>
                      history.push(
                        "aplikacijos/ " + job.id + " ?header=Aplikacijos"
                      )
                    }
                  />
                </td>
              </tr>
            ))}
          </Table>
        </>
      )}
    </div>
  );
};

export default MyPosts;
