import React, { useState, useEffect } from "react";
import LoginFormWrapperBackground from "../../inc/icons/LoginFormBackground.png";
import styled from "styled-components";
import Button from "../../Components/Button";
const Modal = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: white;
  padding: 2% 1%;
  z-index: 9999999999;
  background-image: url(${LoginFormWrapperBackground});
  border-radius: 40px 40px 40px 40px;
  background-size: 100% 100%;
  background-repeat: no-repeat;
  width: 400px;
  min-height: 500px;
  display: flex;
  box-shadow: 0px 0px 24px 0px rgba(0, 0, 0, 0.75);
  .message {
    background: white;
    color: black;
    flex: 1;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 40px 40px 40px 40px;
    div {
      text-align: center;
      p {
        font-size: 18px;
        padding: 0 3px;
      }
      button {
        margin-top: 20%;
      }
    }
  }
`;

const Dialog = ({ open, to, func, header, text, btnText }) => {
  const selectApp = async () => {
    const result = window.document.getElementsByClassName("App")[0];
    setElement(result);
  };
  useEffect(() => {
    selectApp();
  }, []);
  const [element, setElement] = useState(false);

  if (element !== undefined) {
    if (open) {
      element.classList.add("popup");
    }
  }

  return (
    <>
      {open ? (
        <Modal>
          <div className="message">
            <div>
              <h1>{header}</h1>
              <p>{text}</p>
              <Button
                text={btnText ? btnText : "Tęsti"}
                backgroundColor="#A0C334"
                color="white"
                to={to}
                onClick={() => func()}
              />
            </div>
          </div>
        </Modal>
      ) : (
        <></>
      )}
    </>
  );
};

export default Dialog;
