import React, { useEffect, useState } from "react";
import axios from "axios";
import styled from "styled-components";
import icon from "../../inc/icons/folder.svg";
import { useHistory } from "react-router-dom";
import Helmet from "react-helmet";
const MyApplications = () => {
  let Profile = JSON.parse(sessionStorage.getItem("user"));
  let token = JSON.parse(sessionStorage.getItem("token"));

  const [applications, setApplications] = useState(false);

  const Table = styled.table`
    width: 100%;
    text-align: left;
    border-collapse: collapse;
    font-size: 18px;
    td,
    th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #94b42a;
      color: white;
    }
    img {
      cursor: pointer;
    }
    .Drop_List {
      height: 100%;
      width: 100%;
    }
    input {
      font-size: 20px;
      cursor: pointer;
      text-transform: uppercase;
      text-align: center;
      ::placeholder {
        color: white;
      }
    }
    .Drop_Down {
      width: 91%;
      text-transform: uppercase;
    }
  `;
  const Fetch = async () => {
    const res = await axios.get(
      "http://185.34.52.143:1337/applications?_sort=created_at:desc&user.id=" +
        Profile.id,
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    );
    setApplications(res.data);
  };

  useEffect(() => {
    Fetch();
  }, []);
  let regexForData = /[^T]*/;
  let history = useHistory();

  console.log(applications);
  return (
    <div>
      {!applications || applications.length === 0 ? (
        <h1 style={{ textAlign: "cetner" }}>Jus neturite aplikaciju</h1>
      ) : (
        <>
          <Helmet>
            <title>Mano Aplikacijų Statusas</title>
            <meta
              name="description"
              content="Raskite savo aplikacijas kurias pildėte į darbo pozicijas. Taip pat matykite jų statusą, kad sužinotumėte are jūsų anketa buvo - perskaityta, atmesta, arba ją darbdavys pažymėjo kaip mėgstamiausią"
            />
          </Helmet>
          <Table>
            <tr>
              <th>ID</th>
              <th>Kompanija</th>
              <th>Pozicija</th>
              <th>Darbo sfera</th>
              <th>Aplikuota</th>
              <th>Statusas</th>
            </tr>
            {applications.map((app, index) => (
              <tr>
                {app.jobpost !== null ? (
                  <>
                    <td style={{ textAlign: "center" }}>{index}</td>
                    <td>{app.jobpost.name}</td>
                    <td
                      onClick={() => {
                        history.push(`/skelbimas/${app.jobpost.id}`);
                      }}
                      style={{ textDecoration: "underline", cursor: "pointer" }}
                    >
                      {app.jobpost.jobposition}
                    </td>
                    <td>{app.jobpost.jobtype}</td>
                    <td style={{ textAlign: "center" }}>
                      {app.created_at.match(regexForData)}
                    </td>
                    <td>
                      <p
                        style={{
                          backgroundColor:
                            app.status === "Perskaitytas"
                              ? "gold"
                              : app.status === "Megstamiausias"
                              ? "#A0C334"
                              : app.status === "Atmestas"
                              ? "red"
                              : "#FFFFFF",
                          textTransform: "uppercase",
                          textAlign: "center",
                          padding: "2px 0",
                          fontWeight: "bold",
                          color: "#023423",
                          border: "1px solid black"
                        }}
                      >
                        {app.status}
                      </p>
                    </td>
                  </>
                ) : (
                  <></>
                )}
              </tr>
            ))}
          </Table>
        </>
      )}
    </div>
  );
};

export default MyApplications;
