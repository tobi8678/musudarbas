import React, { useState } from "react";
import useForm from "react-hook-form";
import axios from "axios";
import PropTypes from "prop-types";
import styled from "styled-components";
import DropList from "../../Components/DropList";
import SubmitButton from "../../Components/Button";

import ltJson from "../../inc/json/lt.json";
import upload from "../../inc/icons/upload.svg";
import Helmet from "react-helmet";
import Modal from "../Dialog";
const JobPostWrapper = styled.div`
  display: block;
  box-shadow: 0px 0px 8px -2px rgba(0, 0, 0, 0.75);
  border-radius: 25px;
  padding: 2%;
  form {
    .Button_Wrapper {
      margin-top: 5%;
      display: flex;
      justify-content: center;
    }
  }
`;
const Logo = styled.div`
  height: 200px;
  width: 200px;
  border: 1px solid black;
  border-radius: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
  justify-content: center;
  position: relative;
  input {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  }
  label {
    display: block;
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
    height: 120px;
    width: auto;
    background-image: url(${upload});
    cursor: pointer;
    color: transparent;
  }
  p {
    padding-top: 5px;
  }
  img {
    position: absolute;
    height: 100%;
    width: 100%;
    pointer-events: none;
  }
`;
const AboutCompanyWrapper = styled.div`
  display: flex;
  align-items: center;
`;
const AboutCompany = styled.div`
  width: 78%;
  height: 100%;

  div {
    padding-left: 8%;
    label {
      font-size: 20px;
      text-transform: uppercase;
    }
    input {
      width: 100%;
      margin: 10px 0;
      padding: 10px;
      border: none;
      border-radius: 5px;
      background-color: #f7f7f7;
      border-color: #f7f7f7;
      font-size: 20px;
    }
  }
`;
const CompanyDescription = styled.div`
  margin-top: 20px;
  label {
    font-size: 20px;
    text-transform: uppercase;
  }
  textarea {
    padding: 10px;
    font-size: 20px;
    margin-top: 10px;
    height: 170px;
    background-color: #f7f7f7;
    border-color: #f7f7f7;
    width: 100%;
  }
`;
const MoreInformation = styled.div`
  label {
    font-size: 20px;
    text-transform: uppercase;
  }
  input {
    width: 100%;
    margin: 10px 0;
    padding: 10px;
    border: none;
    border-radius: 5px;
    background-color: #f7f7f7;
    border-color: #f7f7f7;
    font-size: 20px;
  }
  h1:nth-child(1) {
    text-align: center;
  }
`;
const MoreAboutJob = styled.div`
  display: table-cell;
  width: 40%;
  padding-right: 50px;
  vertical-align: top;
`;
const Salary = styled.div`
  display: table-cell;
  width: 55%;
  margin-left: 5%;
  .salary-editor {
    width: 45%;
    display: inline-block;
    &:nth-child(2) {
      margin-left: 8%;
    }
  }
`;
const JobPost = props => {
  const [open, setOpen] = useState(false);
  const [image, setImage] = useState("");

  var cities = [];

  for (let i = 0; i < ltJson.length; i++) {
    cities.push(ltJson[i].city);
  }

  const selectedFile = e => {
    var reader = new FileReader();
    reader.onload = function() {
      setImage(reader.result);
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const { register, handleSubmit } = useForm();

  const onSubmit = data => {
    let token = JSON.parse(sessionStorage.getItem("token"));
    let userData = JSON.parse(sessionStorage.getItem("user"));
    data.user = {
      id: userData.id
    };
    data.confirmed = false;
    console.log(data);
    axios
      .post("http://185.34.52.143:1337/jobposts", data, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(response => {
        // Handle success.
        console.log("Data: ", response.data);
        setOpen(true);
      })
      .catch(error => {
        // Handle error.
        console.log("An error occurred:", error);
      });
  };

  return (
    <JobPostWrapper>
      <Modal
        open={open}
        func={() =>
          (window.location.href = "/panele/manoskelbimai?header=Mano Skelbimai")
        }
        header="Skelbimas Sukurtas!"
        text="Jūs sėkmingai sukūrėte darbo skelbimą. Dabar turite laukti, kol Administratorius peržiūrės šį skelbimą. Savo skelbimų statusą, bei aplikacijas į jas galite rasti valdymo panelėje"
      />
      <Helmet>
        <title>Sukurti Darbo Skelbimą</title>
        <meta
          name="description"
          content="Kurti darbo skelbimą, bei jį valdyti."
        />
      </Helmet>
      <form onSubmit={handleSubmit(onSubmit)}>
        <AboutCompanyWrapper>
          <Logo>
            {image !== "" ? <img src={image} /> : ""}

            <div>
              <label for="file">Choose a file</label>
              <input
                type="file"
                name="file"
                id="file"
                onChange={e => selectedFile(e)}
                ref={register}
              />
              <p>ĮKELTI LOGOTIPĄ</p>
            </div>
          </Logo>
          <AboutCompany>
            <div>
              <label>Imones pavadinimas</label>
              <input type="text" name="name" ref={register} required></input>
              <label>Ieskomos pareigos</label>
              <input
                type="text"
                name="jobposition"
                ref={register}
                required
              ></input>
            </div>
          </AboutCompany>
        </AboutCompanyWrapper>
        <CompanyDescription>
          <label>Kompanijos aprasymas</label>
          <textarea name="description" ref={register} required></textarea>
        </CompanyDescription>
        <MoreInformation>
          <h1>PAPILDOMA INFORMACIJA</h1>
          <MoreAboutJob>
            <label>DARBO RŪŠIS</label>
            <DropList
              warning="Sistemoje tokios darbo rusies nera, pasirinkite is saraso"
              array={["It", "Marketingas"]}
              placeholder="DARBO RUSIS"
              refFunc={register}
              name="jobtype"
              req={true}
            />
            <label>DARBO VIETA (MIESTAS)</label>
            <DropList
              array={cities}
              placeholder="DARBO VIETA"
              name="city"
              warning="Toks miestas neegzistuoja"
              refFunc={register}
              req={true}
            />
            <label>PARAISKU TERMINAS (Menesiai)</label>
            <DropList
              array={[1, 2, 3]}
              placeholder="TERMINAS"
              read={true}
              name="period"
              warning="Nepasirinktas periodas"
              refFunc={register}
              req={true}
            />
          </MoreAboutJob>
          <Salary>
            <div className="salary-editor">
              <label>PARAISKU TERMINAS (Menesiai)</label>
              <DropList
                array={["Pilnas etatas", ["Nepilnas etatas"]]}
                placeholder="Etatas"
                read={true}
                name="time"
                refFunc={register}
                warning="Nepasirinktas etato tipas"
                req={true}
              />
            </div>
            <div className="salary-editor">
              <label>Atlyginimas (Eurais)</label>
              <input
                placeholder="1000"
                type="text"
                name="salary"
                ref={register}
                required
              ></input>
            </div>
            <CompanyDescription>
              <label>DARBO Reikalavimai</label>
              <textarea name="jobreq" ref={register} required></textarea>
            </CompanyDescription>
          </Salary>
        </MoreInformation>
        <SubmitButton
          text="skelbti DARBO PASIŪLYMĄ"
          backgroundColor="#A0C334"
          color="white"
        />
      </form>
    </JobPostWrapper>
  );
};

JobPost.propTypes = {};

export default JobPost;
