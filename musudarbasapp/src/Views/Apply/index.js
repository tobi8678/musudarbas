import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Button from "../../Components/Button";
import styled from "styled-components";

import Calendar from "../../inc/icons/calendar.svg";
import Location from "../../inc/icons/navigation.svg";
import LeftArrow from "../../inc/icons/left-arrow.svg";
import Logo from "../../inc/icons/musudarbas3.png";
import axios from "axios";
import { BrowserRouter, useParams, useHistory } from "react-router-dom";
import useForm from "react-hook-form";
import email from "../../inc/icons/email.svg";
import phone from "../../inc/icons/phone.png";
import Helmet from "react-helmet";
import Input from "../../Components/InputWithType";
import Check from "../../Components/CheckBox";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEnvelope,
  faPhone,
  faFileAlt,
  faTrashAlt,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import Modal from "../Dialog";
import "../../inc/fonts/style.css";
import CheckBox from "../../Components/CheckBox";

let Profile = JSON.parse(sessionStorage.getItem("user"));
let token = JSON.parse(sessionStorage.getItem("token"));

const JobPostingDetailedWrapper = styled.div`
  display: block;
  box-shadow: 0px 0px 49px -33px rgba(0, 0, 0, 0.75);
  background: linear-gradient(
    180deg,
    rgba(129, 162, 27, 1) 32%,
    rgba(247, 247, 247, 1) 34%
  );
  padding: 3% 9%;
  margin: 0 4%;
`;
const JobPostingHeader = styled.div`
  position: relative;
  height: 320px;
  padding: 0 2%;
  border-radius: 40px 40px 0px 0px;
  box-shadow: 0px 3px 49px -33px rgba(0, 0, 0, 0.75);
  background-color: #f7f7f7;
  .jobposting_logo {
    height: 300px;
    width: auto;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

const BackButton = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  p {
    margin-left: 20px;
    font-size: 34px;
  }
`;

const JobPopstingContent = styled.div`
  background-color: #f7f7f7;
  position: relative;
  overflow: hidden;
  transition: transform 0.5s;
  transition: height 0.5s;
  .faded {
    position: absolute;
    transform: translateX(3000px);
    height: 0;
  }
  .jobapply_header {
    text-align: center;
    font-weight: light;
    margin: 0;
    padding: 21px 0;
    span {
      font-weight: bold;
      text-transform: uppercase;
    }
  }
  .jobapply_steps {
    display: flex;
    justify-content: center;
    .steps {
      width: 20%;
      ul {
        list-style: none;
        display: flex;

        padding: 0;
        justify-content: space-evenly;
        li {
          display: inline-block;
          padding: 30px;
          font-weight: bold;
          font-size: 20px;
          background-color: gray;
          color: white;
        }
      }
    }
  }
  .step_header {
    text-align: center;
  }
  .StepOne {
    position: relative;
    overflow: hidden;
    .mycontacts {
      position: absolute;
      transform: translateX(5000px);
    }
    .StepOne_newcontacts {
      transition: transform 0.5s;
      display: flex;
      justify-content: center;
      form {
        input {
          display: block;
          margin-top: 10px;
        }
      }
    }
    .StepOne_oldcontacts {
      .use_my_contacts {
        display: flex;
        justify-content: center;
        flex-wrap: nowrap;
        align-items: center;
        .contact {
          display: flex;
          align-items: center;
          p {
            margin-left: 20px;
            font-size: 20px;
          }
        }
      }
      h1 {
        text-align: center;
      }
      .checkBox {
        display: flex;
        justify-content: center;
        align-items: center;
        p {
          font-size: 20px;
          margin-left: 20px;
        }
      }
    }
  }
  .StepTwo {
    .description {
      text-align: center;
      font-size: 20px;
    }
    textarea {
      font-size: 20px;
      color: black;
      width: 600px;
      min-height: 400px;
      display: block;
      margin: 0 auto;
    }
  }
`;
const JobPostingFooter = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 2%;
  background-color: #f7f7f7;
  border-radius: 0px 0px 40px 40px;
  padding-top: 20px;
  padding-bottom: 20px;
  margin-top: 40px;
  button {
    margin: 0 10px;
  }
`;

const Cvbank = styled.div`
  padding: 0 10%;
  form {
    display: flex;
    align-items: center;
    width: auto;
    justify-content: flex-end;
    input {
      border: 1px solid black;
    }
    svg {
      padding-left: 3%;
      height: 40px;
      width: 40px !important;
      cursor: pointer;
    }
    #file {
      display: none;
    }
    label {
      margin: 0 2%;
      display: flex;
      justify-content: center;
      svg {
        transition: color 0.5s;
        :hover {
          color: #a0c334;
        }
      }
    }
  }
  .CVbank_header {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .checkTheCv {
    display: flex;
  }
  a {
    width: 100%;
    margin-left: 20px;
  }
  .CV {
    padding: 0 1%;
    margin: 1% 0;
    display: flex;
    justify-content: space-between;
    align-items: center;
    border: 1px solid black;
    border-radius: 20px;
    transition: padding 0.5s;
    p {
      font-size: 20px;
      margin-left: 3%;
      transition: font-size 0.5s;
    }
    svg {
      color: salmon;
      height: 30px;
      width: 30px;
      transition: height 0.5s;
      transition: width 0.5s;
    }
    &_icon {
      display: flex;
      align-items: center;
      width: 100%;
    }
    :hover {
      cursor: pointer;
      padding: 0.5% 1%;
      svg {
        height: 32px;
        width: 32px;
      }
      p {
        font-size: 22px;
      }
    }
  }
`;

const Apply = ({ postedDate, logo, deadline }) => {
  let { id } = useParams();

  const [post, setPost] = useState("");
  const [steps, setStep] = useState(1);
  const [check, setCheck] = useState(false);
  const [filename, setFilename] = useState("");
  const [cvresult, setCvResults] = useState(false);
  const [cv, setCv] = useState(false);
  const { register, handleSubmit, watch, errors } = useForm();
  const [selectedFile, setSelectedFile] = useState(0);
  const [selectedCv, setSelectedCv] = useState([]);

  const [emailValue, setEmailValue] = useState("");
  const [phoneValue, setPhoneValue] = useState("");
  const [moreInfo, setMoreInfo] = useState("");

  const sendApplication = () => {
    let send = {};
    if (!check) {
      send.email = emailValue;
      send.phone = phoneValue;
    } else {
      send.email = user.email;
      send.phone = "+370000000";
    }
    send.cvs = [
      {
        id: selectedCv
      }
    ];
    send.moreinformation = moreInfo;
    send.user = user.id;
    send.jobpost = id;

    axios
      .post("http://185.34.52.143:1337/applications", send, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(response => {
        setOpen(true);
      });
    console.log(send);
  };

  const selectCv = data => {
    setSelectedCv(data);
    console.log(selectedCv);
  };

  const getBase64 = (file, cb) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function() {
      cb(reader.result);
    };
  };
  const deleteCv = id => {
    axios
      .delete("http://185.34.52.143:1337/cvs/" + id, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(() => {
        window.location.reload();
      });
  };
  const onSubmit = data => {
    console.log(data.file[0]);
    getBase64(data.file[0], result => {
      axios
        .post(
          "http://185.34.52.143:1337/cvs",
          { name: data.name, file: result, user: Profile.id },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        .then(result => {
          window.location.reload();
        });
    });
  };

  let stepHeader = "";

  if (steps == 1) {
    stepHeader = "Kontaktai";
  }
  if (steps == 2) {
    stepHeader = "CV";
  }
  if (steps == 3) {
    stepHeader = "Papildoma informacija";
  }

  let numbers = [1, 2, 3];

  let user = JSON.parse(sessionStorage.getItem("user"));
  const fetchData = async () => {
    const result = await axios("http://185.34.52.143:1337/jobposts/" + id);
    setPost(result.data);
    if (Profile !== null) {
      const FetchCv = await axios(
        "http://185.34.52.143:1337/cvs?_sort=created_at:desc&user.id=" +
          Profile.id,

        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      );
      setCvResults(FetchCv.data);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const [open, setOpen] = useState(false);
  console.log(post);
  return (
    <JobPostingDetailedWrapper>
      {post === "" ? (
        <></>
      ) : (
        <>
          <JobPostingHeader>
            <Helmet>
              <title>Aplikacija pozicijai: {post.jobposition}</title>
              <meta
                name="description"
                content={`Aplikuoti ${post.jobposition}. Kompanija : ${post.name}`}
              />
            </Helmet>
            <BackButton onClick={() => window.history.back()}>
              <img src={LeftArrow} />
              <p>GRĮŽTI ATGAL</p>
            </BackButton>
            <img className="jobposting_logo" src={logo ? logo : Logo} />
          </JobPostingHeader>
          <JobPopstingContent>
            {Profile === null ? (
              <>
                <h1 style={{ textAlign: "Center" }}>
                  Prisijunkite jeigu norite aplikuoti siam darbui
                </h1>
              </>
            ) : (
              <>
                <div className="jobapply_header">
                  <h1>
                    Aplikacija i: <span>{post.name}</span>, pozicijai:{" "}
                    <span>{post.jobposition}</span>
                  </h1>
                </div>
                <div className="jobapply_steps">
                  <div className="steps">
                    <ul>
                      {numbers.map(number => (
                        <li
                          style={{
                            backgroundColor: number === steps ? "#A0C334" : ""
                          }}
                        >
                          {number}
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>

                <h1 className="step_header">
                  {" "}
                  {steps} - Zingsins: {stepHeader}
                </h1>
                <div className={steps === 1 ? "StepOne" : "StepOne faded"}>
                  <div
                    className={
                      check
                        ? "StepOne_newcontacts mycontacts"
                        : "StepOne_newcontacts"
                    }
                  >
                    <form>
                      <Input
                        icon={email}
                        name="email"
                        placeholder="email"
                        value={phoneValue}
                        onChange={e => setEmailValue(e.target.value)}
                        value={emailValue}
                      />
                      <Input
                        icon={phone}
                        name="phone"
                        placeholder="telefono numeris"
                        value={phoneValue}
                        onChange={e => setPhoneValue(e.target.value)}
                      />
                    </form>
                  </div>
                  <div className="StepOne_oldcontacts">
                    <h1>- Arba - </h1>
                    <div className="use_my_contacts">
                      <div>
                        <div className="contact">
                          <FontAwesomeIcon icon={faEnvelope} />{" "}
                          <p>{user.email}</p>
                        </div>
                        <div className="contact">
                          <FontAwesomeIcon icon={faPhone} /> <p>+370000000</p>
                        </div>
                      </div>
                    </div>
                    <div className="checkBox">
                      <Check
                        onClick={() => {
                          setCheck(!check);
                          console.log(check);
                        }}
                      />{" "}
                      <p>Naudoti profilio kontaktus</p>
                    </div>
                  </div>
                </div>

                <div className={steps === 2 ? "StepTwo" : "StepTwo faded"}>
                  <div className="description">
                    <p>
                      Antrajame zingsnyje pasirinkite CV kuri norite prisegti
                      prie savo anketos.
                    </p>
                    <Cvbank>
                      <div className="CVbank_header">
                        <h1>Rinkites is savo CV banko</h1>
                        <div>
                          {!cv ? (
                            <Button
                              text="Ikelti CV"
                              backgroundColor="#A0C334"
                              color="white"
                              onClick={() => {
                                setCv(!cv);
                              }}
                            />
                          ) : (
                            <form onSubmit={handleSubmit(onSubmit)}>
                              <Input
                                type="text"
                                name="name"
                                placeholder="CV pavadinimas"
                                icon={faFileAlt}
                                refFunc={register}
                              />
                              <label for="file">
                                <FontAwesomeIcon icon={faFileAlt} />
                                {filename}
                              </label>
                              <input
                                type="file"
                                id="file"
                                name="file"
                                onChange={e => {
                                  setFilename(e.target.value);
                                }}
                                ref={register}
                              />

                              <Button
                                text="Ikelti"
                                backgroundColor="#A0C334"
                                color="white"
                                onClick={() => {
                                  setCv(!cv);
                                }}
                              />
                              <FontAwesomeIcon
                                icon={faTimes}
                                onClick={() => setCv(!cv)}
                              />
                            </form>
                          )}
                        </div>
                      </div>
                      <hr></hr>

                      {!cvresult ? (
                        <></>
                      ) : (
                        <>
                          {cvresult.length <= 0 ? (
                            <h1>Just neturite nei vieno CV</h1>
                          ) : (
                            cvresult.map(onecv => (
                              <div className="checkTheCv">
                                <CheckBox onClick={() => selectCv(onecv.id)} />
                                <a
                                  style={{ color: "black" }}
                                  download={onecv.name}
                                  href={onecv.file}
                                >
                                  <div className="CV">
                                    <div className="CV_icon">
                                      <FontAwesomeIcon icon={faFileAlt} />
                                      <p>{onecv.name}</p>
                                    </div>
                                    <div className="CV_actions">
                                      <FontAwesomeIcon
                                        icon={faTrashAlt}
                                        onClick={e => deleteCv(onecv.id)}
                                      />
                                    </div>
                                  </div>
                                </a>
                              </div>
                            ))
                          )}
                        </>
                      )}
                    </Cvbank>
                  </div>
                </div>

                <div className={steps === 3 ? "StepTwo" : "StepTwo faded"}>
                  <div className="description">
                    <p>
                      Treciajame zingsnyje pridekite uzrasa (pvz. jusu
                      tinkalalapis).
                    </p>
                  </div>{" "}
                  <textarea
                    value={moreInfo}
                    onChange={e => {
                      setMoreInfo(e.target.value);
                    }}
                  ></textarea>
                </div>
              </>
            )}
          </JobPopstingContent>
          <JobPostingFooter>
            {steps > 1 ? (
              <Button
                backgroundColor="#A0C334"
                color="#FFFFFF"
                text="< Atgal"
                onClick={() => {
                  setStep(steps - 1);
                }}
              />
            ) : (
              <> </>
            )}
            <Button
              backgroundColor="#A0C334"
              color="#FFFFFF"
              text={steps === 3 ? "Siusti anketa" : "Kitas zingsnis"}
              onClick={() => {
                if (steps < 3) {
                  setStep(steps + 1);
                } else if (steps === 3) {
                  sendApplication();
                }
              }}
            />
          </JobPostingFooter>
        </>
      )}
      <Modal
        open={open}
        func={() => (window.location.href = "/skelbimas/" + id)}
        header="Anketa išsiųsta!"
        text="Jūsų aplikacija buvo išsiųsta sėkmingai. Jos statusą galite rasite valdymo panelėje"
      />
    </JobPostingDetailedWrapper>
  );
};

export default Apply;
