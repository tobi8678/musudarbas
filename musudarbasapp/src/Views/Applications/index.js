import React, { useState, useEffect } from "react";
import axios from "axios";
import styled from "styled-components";
import Drop from "../../Components/DropList";
import icon from "../../inc/icons/folder.svg";
import { useHistory } from "react-router-dom";
import Helmet from "react-helmet";
import Modal from "../Dialog";

const Table = styled.table`
  width: 100%;
  text-align: left;
  border-collapse: collapse;
  font-size: 18px;
  td,
  th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }

  tr:nth-child(even) {
    background-color: #94b42a;
    color: white;
  }
  img {
    cursor: pointer;
  }
  .Drop_List {
    height: 100%;
    width: 100%;
  }
  input {
    font-size: 20px;
    cursor: pointer;
    text-transform: uppercase;
    text-align: center;
    ::placeholder {
      color: white;
    }
  }
  .Drop_Down {
    width: 91%;
    text-transform: uppercase;
  }
`;

const Applicants = ({ match }) => {
  let Profile = JSON.parse(sessionStorage.getItem("user"));
  let token = JSON.parse(sessionStorage.getItem("token"));
  const [applications, setApplications] = useState([]);
  const [res, setRes] = useState([]);
  const [id, setId] = useState("");
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  let regexForData = /[^T]*/;
  const fetchData = async () => {
    const Applicants = await axios(
      `http://185.34.52.143:1337/jobposts?id=${match.params.id}&user.id=${Profile.id}`
    );
    const result = await axios(
      `http://185.34.52.143:1337/applications?jobpost.id=${match.params.id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    );

    setApplications(Applicants.data);
    setRes(result.data);
  };
  useEffect(() => {
    fetchData();
  }, []);

  if (
    applications[0] === undefined ||
    applications[0].applications.length === 0
  ) {
  } else {
    console.log(res);
  }
  const onClick = data => {
    axios
      .put(
        "http://185.34.52.143:1337/applications/" + id,
        { status: data },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(() => {
        setMessage("Vartotojo aplikacijos statusas buvo pakeistas į : " + data);
        setOpen(true);
      });
    console.log(res);
  };
  let history = useHistory();
  return (
    <div>
      {applications[0] === undefined ||
      applications[0].applications.length === 0 ? (
        <h1>Sis darbo skelbimas neturi aplikaciju</h1>
      ) : (
        <div>
          <Helmet>
            <title>Aplikacijos pozicijai: {applications[0].jobposition} </title>
            <meta
              name="description"
              content="Aplikacijų valdymas - jų statuso keitimas, bei peržiūrėjimas."
            />
          </Helmet>
          <h1 style={{ textAlign: "center" }}>
            Siai darbo pozicijai : {applications[0].jobposition}, kompanija :{" "}
            {applications[0].name}, darbo sfera : {applications[0].jobtype},
            etatas : {applications[0].time}
          </h1>
          <Table>
            <tr>
              <th>ID</th>
              <th>Varas, Pavarde</th>
              <th>Aplikuota</th>

              <th>Statusas</th>
              <th>Perziureti</th>
            </tr>

            {res.map((user, index) => (
              <tr>
                <td style={{ textAlign: "center" }}>{index}</td>
                <td>{user.user.username}</td>
                <td style={{ textAlign: "center" }}>
                  {user.created_at.match(regexForData)}
                </td>
                <td onClick={() => setId(user.id)}>
                  <Drop
                    array={["Perskaitytas", "Megstamiausias", "Atmestas"]}
                    read
                    placeholder={user.status}
                    onClick={onClick}
                    background={
                      user.status === "Perskaitytas"
                        ? "gold"
                        : user.status === "Megstamiausias"
                        ? "#A0C334"
                        : user.status === "Atmestas"
                        ? "red"
                        : ""
                    }
                  />
                </td>
                <td style={{ display: "flex", justifyContent: "center" }}>
                  <img
                    style={{ height: "28px", width: "28px" }}
                    src={icon}
                    onClick={() =>
                      history.push(
                        match.params.id +
                          "/aplikacija/" +
                          user.id +
                          "?header=Aplikacija"
                      )
                    }
                  />
                </td>
              </tr>
            ))}
          </Table>
        </div>
      )}
      <Modal
        open={open}
        func={() => {
          window.location.reload();
        }}
        header="Statusas pakeistas"
        text={message}
      />
    </div>
  );
};

export default Applicants;
