import React, { useState } from "react";
import {
  BrowserRouter,
  useHistory,
  Route,
  Link,
  Switch,
  useParams,
  useRouteMatch
} from "react-router-dom";
import styled from "styled-components";

import dashboardLogo from "../../inc/icons/dashboardicon.svg";
import JobPost from "../../Views/JobPost";
import ControlJobPost from "../../Views/ControlJobPost";
import Profile from "../../Views/ProfilePage";
import MyPosts from "../../Views/MyPosts";
import Applicants from "../../Views/Applications";
import SingleApplication from "../SingleApplication";
import MyApplications from "../MyApplications";
const DashboardWrapper = styled.div`
  padding: 0 5%;

  position: relative;
`;
const Navigation = styled.div`
  display: table-cell;
  vertical-align: top;
  width: 25vw;
  height: 78vh;
  position: absolute;
  nav {
    background: linear-gradient(
      90deg,
      rgba(160, 195, 52, 1) 0%,
      rgba(143, 175, 39, 1) 29%
    );
    width: 100%;
    min-height: 100% !important;
    ul {
      list-style: none;
      margin: 0;
      padding: 0;
      a {
        color: white;
        text-decoration: none;
        font-size: 19px;
      }
      li {
        width: 93%;
        padding: 8% 0;
        text-align: center;
        border-radius: 0 50px 50px 0;
        transition: background-color 0.2s;
        &:hover {
          background-color: #a0c334;
          cursor: pointer;
        }
        a {
          color: white;
          text-decoration: none;
          font-size: 19px;
        }
      }
    }
  }
`;
const EditorWindow = styled.div`
  width: 65vw;
  right: 5%;
  position: absolute;
  background-color: white;
  border-radius: 25px 0 25px 0;
  p {
    margin: 0;
  }
`;
const DashboardLogo = styled.div`
  margin: 0 auto;
  margin-left: 15%;
  display: flex;
  align-items: center;
  height: 80px;
  box-shadow: 0px 10px 12px -20px rgba(0, 0, 0, 1);
  h1 {
    color: white;
    text-transform: uppercase;
    margin-left: 15px;
    font-size: 22px;
  }
`;
const EditorHeader = styled.div`
  height: 80px;
  box-shadow: 0px 10px 12px -12px rgba(0, 0, 0, 0.75);
  border-bottom: 1px solid rgba(0, 0, 0, 0.2);
  display: flex;
  align-items: center;

  h1 {
    text-transform: uppercase;
    font-weight: bold;
    margin: 0;
    padding: 0;
    margin-left: 8%;
  }
`;
const Editor = styled.div`
  padding: 3%;
  height: 70vh;
  overflow-y: scroll;
`;
const Dashboard = props => {
  let data = JSON.parse(sessionStorage.getItem("user"));
  let history = useHistory();
  let { path, url } = useRouteMatch();

  let header = new URL(window.location.href).searchParams.get("header");

  return (
    <>
      {data === null ? (
        <>{history.push("/prisijungimas")}</>
      ) : (
        <DashboardWrapper>
          <Navigation>
            <DashboardLogo>
              <img src={dashboardLogo} />
              <h1>Valdymo panelė</h1>
            </DashboardLogo>

            <nav>
              <ul>
                <Link to="/panele/profilis?header=Mano Profilis">
                  <li> Mano profilis</li>
                </Link>
                <Link to="/panele/manoaplikacijos?header=Mano Aplikacijos">
                  {" "}
                  <li> Mano aplikacijos </li>
                </Link>
                {data.role.name === "Admin" ? (
                  <Link to="/panele/skelbimuvaldymas?header=Valdykite sukurtus darbo pasiūlymus">
                    <li>Sukurtų darbo pasiūlymų valdymas</li>
                  </Link>
                ) : (
                  <></>
                )}
                <Link to="/panele/manoskelbimai?header=Mano Skelbimai">
                  {" "}
                  <li>Mano darbo skelbimai</li>
                </Link>
                {data.role.name === "Admin" ? (
                  <a href="http://185.34.52.143:1337/admin" target="_blank">
                    {" "}
                    <li>Vartotoju valdymas</li>
                  </a>
                ) : (
                  <></>
                )}
              </ul>
            </nav>
          </Navigation>

          <EditorWindow>
            <EditorHeader>
              <h1>{header}</h1>
            </EditorHeader>
            <Editor id="style-1">
              <Switch>
                <Route
                  exact
                  path={`${path}/aplikacijos/:id`}
                  component={Applicants}
                />
                <Route
                  exact
                  path={`${path}/aplikacijos/:id/aplikacija/:uid`}
                  component={SingleApplication}
                />
                <Route exact path={`${path}/skelbimas`}>
                  <JobPost />
                </Route>
                <Route exact path={`${path}/profilis`} component={Profile} />
                <Route
                  exact
                  path={`${path}/skelbimuvaldymas`}
                  component={ControlJobPost}
                />
                <Route
                  exact
                  path={`${path}/manoaplikacijos`}
                  component={MyApplications}
                />
                <Route
                  exact
                  path={`${path}/manoskelbimai`}
                  component={MyPosts}
                />
              </Switch>
            </Editor>
          </EditorWindow>
        </DashboardWrapper>
      )}
    </>
  );
};

Dashboard.propTypes = {};

export default Dashboard;
