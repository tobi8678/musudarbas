import React, { useState, useEffect } from "react";
import useForm from "react-hook-form";
import { useHistory } from "react-router-dom";
import JobPost from "../../Views/JobPosting";
import Logo from "../../inc/icons/musudarbas3.png";
import styled from "styled-components";
import axios from "axios";
import Drop from "../../Components/DropList";
import ltJson from "../../inc/json/lt.json";
import location from "../../inc/icons/locationIcon.svg";
import arrow from "../../inc/icons/whiteArrow.svg";

import Button from "../../Components/Button";
const AllJobPosts = styled.div`
  padding: 0 5%;
`;
const HeadLine = styled.header`
  text-align: center;
  h1 {
    color: white;
    font-weight: 100;
    span {
      font-weight: bold;
    }
  }
`;
const PostWrapper = styled.div``;
const Posts = styled.div`
  min-height: 600px;
  background-color: #f7f7f7;
  .posts_window-columns {
    display: inline-block;
    &:nth-child(1) {
      width: 20%;
    }
    &:nth-child(2) {
      width: 60%;
    }
    &:nth-child(3) {
      width: 20%;
    }
  }
`;
const Filter = styled.div`
  color: white;
  text-align: center;
  background-color: #77951d;
  padding: 40px 0;
  h3 {
    font-size: 30px;
    margin: 0;
  }
  form {
    margin-top: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    button {
      margin-left: 50px;
    }
    #jobName {
      padding: 20px 20px;
      border-bottom-left-radius: 20px;
      border-top-left-radius: 20px;
    }
    #jobSector {
      width: 150px;
      input {
        width: 150px;
        padding: 20px 20px;
        border-bottom-right-radius: 20px;
        border-top-right-radius: 20px;
        border-left: 2px solid white;
      }
    }
    #jobCity {
      width: 250px;
      margin-left: 50px;

      input {
        padding: 20px 20px;
        border-radius: 20px;
      }
    }
    input {
      font-size: 21px;
      color: white;
      background-color: #a0c334;
      border: none;
      ::placeholder {
        color: white;
      }
    }
    .Drop_Down {
      background-color: #a0c334;
      width: 100%;
      li {
        color: white;
      }
    }
  }
`;
const PolularPosts = styled.div``;
const RegularPosts = styled.div`
  .jobpost_block {
    margin: 3% 0;
  }
`;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding-bottom: 2%;
  .Button_Wrapper {
    margin-left: 15px;
    button {
      padding: 20px 30px;
      :hover {
        background-color: #81a21b;
      }
    }
  }
`;
const Adds = styled.div``;
const AllJobs = () => {
  const [posts, setPosts] = useState(false);
  const [count, setCount] = useState(false);
  const [, setPage] = useState(0);
  const [ip, setip] = useState("");
  const [filterString, setFilterString] = useState("");
  const { register, handleSubmit } = useForm();

  let numberOfPages = count.length / 6;
  if (!Number.isInteger(numberOfPages)) {
    numberOfPages += 1;
  }
  let history = useHistory();

  const Buttons = [];
  let urlFilter = new URL(window.location.href).searchParams.get("filter");
  if (urlFilter === null) {
    urlFilter = 0;
  }
  for (let i = 1; i <= numberOfPages; i++) {
    Buttons.push(
      <Button
        text={i}
        backgroundColor={urlFilter / 3 === i ? "#81A21B" : "#90B028"}
        color="white"
        onClick={() => {
          i > 1 ? setPage((i - 1) * 6) : setPage(0);
          console.log(i);
          history.push("?&filter=" + (i - 1) * 6 + extraFilterString);
          window.location.reload();
        }}
      />
    );
  }
  let string = "";
  const onSubmitFilter = data => {
    if (data.name.length > 0) {
      setFilterString(filterString + "&name_contains=" + data.name);
      string += "&name=" + data.name;
    }

    if (data.jobtype.length > 0) {
      setFilterString(filterString + "&jobtype_contains=" + data.jobtype);
      string += "&jobtype=" + data.jobtype;
    }

    if (data.city.length > 0) {
      setFilterString(filterString + "&city_contains=" + data.city);
      string += "&city=" + data.city;
    }
    history.push("/?" + string);
    window.location.reload();
  };
  let urlFilterCity = new URL(window.location.href).searchParams.get("city");
  let urlFilterName = new URL(window.location.href).searchParams.get("name");
  let urlJobType = new URL(window.location.href).searchParams.get("jobtype");

  let filterCombainedString = "";
  let extraFilterString = "";
  if (urlFilterCity === null) {
    urlFilterCity = "";
  } else {
    filterCombainedString += "&city_contains=" + urlFilterCity;
    extraFilterString += "&city=" + urlFilterCity;
  }
  if (urlFilterName === null) {
    urlFilterName = "";
  } else {
    filterCombainedString += "&name_contains=" + urlFilterName;
    extraFilterString += "&name=" + urlFilterName;
  }
  if (urlJobType === null) {
    urlJobType = "";
  } else {
    filterCombainedString += "&jobtype_contains=" + urlJobType;
    extraFilterString += "&jobtype=" + urlJobType;
  }
  const fetchData = async () => {
    const result = await axios(
      "http://185.34.52.143:1337/jobposts?confirmed=true&_start=" +
        urlFilter +
        filterCombainedString +
        "&_limit=6&_sort=created_at:DESC"
    );
    const counter = await axios(
      "http://185.34.52.143:1337/jobposts?" +
        filterCombainedString +
        "&confirmed=true/count"
    );
    const ip = await axios("http://ip-api.com/json/");
    setip(ip.data.query);
    setPosts(result.data);
    setCount(counter.data);
  };
  useEffect(() => {
    fetchData();
  }, []);
  var cities = [];

  for (let i = 0; i < ltJson.length; i++) {
    cities.push(ltJson[i].city);
  }
  return (
    <AllJobPosts>
      {!posts ? (
        <></>
      ) : (
        <>
          {" "}
          <HeadLine>
            <h1>
              SURASK DARBĄ <span>GREIČIAU</span> IR <span>EFEKTYVIAU</span>
            </h1>
          </HeadLine>
          <PostWrapper>
            <Filter>
              <h3>Įvesk savo norimus paieškos kriterijus</h3>
              <form onSubmit={handleSubmit(onSubmitFilter)}>
                <input
                  id="jobName"
                  name="name"
                  ref={register}
                  placeholder={
                    urlFilterName === "" ? "Darbo pavadinimas" : urlFilterName
                  }
                />
                <Drop
                  id="jobSector"
                  array={["Marketingas", "it"]}
                  placeholder={urlJobType === "" ? "Sektorius" : urlJobType}
                  icon={arrow}
                  name="jobtype"
                  refFunc={register}
                />
                <Drop
                  id="jobCity"
                  array={cities}
                  placeholder={urlFilterCity === "" ? "Miestas" : urlFilterCity}
                  icon={location}
                  name="city"
                  refFunc={register}
                />
                <Button text="PRADĖTI" />
              </form>
              {!count ? (
                <></>
              ) : (
                <h3 style={{ marginTop: "15px" }}>
                  Rezultatai : {count.length}
                </h3>
              )}
            </Filter>
            <Posts>
              <PolularPosts className="posts_window-columns">
                Popular
              </PolularPosts>
              <RegularPosts className="posts_window-columns">
                {posts !== null
                  ? posts.map(post => (
                      <JobPost
                        companiesLogo={Logo}
                        companyName={post.jobposition}
                        descriptionExpert={post.description}
                        jobType={post.time}
                        location={post.city}
                        postedDate={3}
                        navigateTo={`/skelbimas/${post.id}`}
                        id={post.id}
                        ip={ip}
                      />
                    ))
                  : ""}
              </RegularPosts>
              <Adds className="posts_window-columns">Adds</Adds>
              <ButtonsWrapper>{Buttons}</ButtonsWrapper>
            </Posts>
          </PostWrapper>
        </>
      )}
    </AllJobPosts>
  );
};

AllJobs.propTypes = {};

export default AllJobs;
