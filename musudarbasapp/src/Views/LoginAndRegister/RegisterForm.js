import React, { useState } from "react";
import useForm from "react-hook-form";
import axios from "axios";
import styled from "styled-components";

import CheckBox from "../../Components/CheckBox";
import InputWithType from "../../Components/InputWithType";
import SubmitButton from "../../Components/Button";

import passWordIcon from "../../inc/icons/password.svg";
import envelopeIcon from "../../inc/icons/envelope.svg";
import surnameIcon from "../../inc/icons/surname.svg";
import avatarIcon from "../../inc/icons/avatar.svg";
import Modal from "../Dialog";

const Form = styled.form`
  padding-top: 20px;
  input {
    width: 100%;
    margin-top: 20px;
  }
  .Button_Wrapper {
    display: flex;
    justify-content: center;
    padding: 60px 0;
  }
  .errorMessage {
    color: salmon;
    text-align: center;
  }
`;
const CheckoBoxWrapper = styled.div`
  margin-top: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Error = styled.p`
  text-align: center;
  margin: 0;
  font-size: 14px;
`;
const LoginFormWrapper = styled.div`
  padding: 0 20px;
  height: 100%;
`;
const ForgotThePassword = styled.p`
  text-align: center;
  font-size: 20px;
`;
const Rules = styled.p`
  margin: 0;
  margin-left: 20px;
  font-size: 24px;
`;

const RegisterForm = () => {
  const [open, setOpen] = useState(false);
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = data => {
    axios
      .post("http://185.34.52.143:1337/auth/local/register", {
        username: data.name + " " + data.surname,
        email: data.email,
        password: data.password
      })
      .then(response => {
        console.log(["Sveiki prisijunge!", response, data]);
        setOpen(true);
      })
      .catch(error => {
        // Handle error.
        console.log("An error occurred:", error);
      });
  };

  return (
    <>
      <Modal
        open={open}
        func={() => {
          window.location.reload();
        }}
        header="Prisiregistruota"
        text="Jūs sėkmingai prisiregistravote, dabar galite prisijungti!"
      />
      <LoginFormWrapper>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <InputWithType
            icon={avatarIcon}
            placeholder="Vardas"
            name="name"
            type="text"
            refFunc={register({ required: true })}
          />
          {errors.name && (
            <p className="errorMessage">*Nepalikite laukelio tuscio</p>
          )}
          <InputWithType
            icon={surnameIcon}
            placeholder="Pavardė"
            name="surname"
            type="text"
            refFunc={register({ required: true })}
          />
          {errors.surname && (
            <p className="errorMessage">*Nepalikite laukelio tuscio</p>
          )}
          <InputWithType
            icon={envelopeIcon}
            placeholder="Elektroninis paštas"
            name="email"
            type="text"
            refFunc={register({ required: true })}
          />
          {errors.email && (
            <p className="errorMessage">*Nepalikite laukelio tuscio</p>
          )}
          <InputWithType
            icon={passWordIcon}
            placeholder="Slaptažodis"
            name="password"
            type="password"
            refFunc={register({ required: true })}
          />
          {errors.password && (
            <p className="errorMessage">*Nepalikite laukelio tuscio</p>
          )}
          <CheckoBoxWrapper>
            <CheckBox />
            <Rules>Sutinku su taisyklėmis ir sąlygomis </Rules>
          </CheckoBoxWrapper>

          <SubmitButton text="Registruotis" type="submit" />
        </Form>
      </LoginFormWrapper>
    </>
  );
};

export default RegisterForm;
