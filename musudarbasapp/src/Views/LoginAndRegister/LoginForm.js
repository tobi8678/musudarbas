import React, { useState } from "react";
import useForm from "react-hook-form";
import axios from "axios";

import styled from "styled-components";

import Logotype from "../../inc/icons/musudarbas3.png";
import InputWithType from "../../Components/InputWithType";
import SubmitButton from "../../Components/Button";

import passWordIcon from "../../inc/icons/password.svg";
import envelopeIcon from "../../inc/icons/envelope.svg";

const Form = styled.form`
  input {
    width: 100%;
    margin-top: 10px;
  }
  .Button_Wrapper {
    display: flex;
    justify-content: center;
    padding: 60px 0;
  }
`;

const Logo = styled.img`
  height: 200px;
  width: auto;
  display: block;
  margin: auto;
  padding: 20px 0;
`;
const Error = styled.p`
  text-align: center;
  margin: 0;
  font-size: 14px;
`;
const LoginFormWrapper = styled.div`
  padding: 0 20px;
  height: 100%;
`;
const ForgotThePassword = styled.p`
  text-align: center;
  font-size: 20px;
`;
const LoginForm = ({ clicked }) => {
  const [error, setError] = useState("");
  const { register, handleSubmit } = useForm();
  const onSubmit = data => {
    axios
      .post("http://185.34.52.143:1337/auth/local", {
        identifier: data.email,
        password: data.password
      })
      .then(response => {
        // Handle success.
        sessionStorage.setItem("user", JSON.stringify(response.data.user));
        sessionStorage.setItem("token", JSON.stringify(response.data.jwt));
        console.log("Well done!");
        window.location.reload();
      })
      .catch(error => {
        // Handle error.
        console.log("An error occurred:", error);
        setError(
          "* Neteisingai įvestas elektroninis paštas arba slaptažodis. Prašome bandyti dar kartą!"
        );
      });
  };
  return (
    <LoginFormWrapper>
      <Logo src={Logotype} />
      <Error>{error}</Error>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <InputWithType
          icon={envelopeIcon}
          placeholder="Elektroninis paštas"
          name="email"
          type="text"
          refFunc={register}
        />
        <InputWithType
          icon={passWordIcon}
          placeholder="Slaptažodis"
          name="password"
          type="password"
          refFunc={register}
        />
        <ForgotThePassword>Pamiršote slaptažodį?</ForgotThePassword>
        <SubmitButton text="Prisijungti" navigateTo="/" onClick={clicked} />
      </Form>
    </LoginFormWrapper>
  );
};

export default LoginForm;
