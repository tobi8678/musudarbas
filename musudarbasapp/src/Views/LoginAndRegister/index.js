import React, { useState } from "react";
import styled from "styled-components";

import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";
import "../../inc/popup.css";
import { Helmet } from "react-helmet";

import LoginFormWrapperBackground from "../../inc/icons/LoginFormBackground.png";

const LoginFormBackground = styled.div`
  padding: 30px;
  width: 700px;
  display: flex;
  justify-content: center;
  background-image: url(${LoginFormWrapperBackground});
  border-radius: 40px 40px 40px 40px;
  background-size: 100% 97%;
  background-repeat: no-repeat;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 1000;
  box-shadow: 10px 14px 42px -10px rgba(0, 0, 0, 0.53);
`;
const LoginFormWrapper = styled.div`
  display: block;
  width: 600px;
  background-color: #f7f7f7;
  border-radius: 40px 40px 40px 40px;
`;
const HeaderWithSwitch = styled.div`
  justify-content: space-around;
  background: #8faf27;
  border-radius: 40px 40px 0 0;
  padding-top: 20px;
  display: flex;
  flex-wrap: wrap;
  button {
    background: transparent;
    border: none;
    cursor: pointer;
    color: white;
    font-size: 30px;
    text-transform: uppercase;
    transition: font-weight 0.6s;
  }
`;

const Form = styled.div`
  min-height: 670px;
`;
const HeaderButtonWrapper = styled.div`
  display: block;
`;
const ActivePageIndicator = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-around;
  margin-top: 12px;
`;
const Indicator = styled.div`
  display: block;
  width: 60px;
  height: 8px;
  background: #c1ff00;
`;
const LoginAndRegister = ({ clicked }) => {
  const [page, setPage] = useState("login");

  const switchPage = swtichPage => {
    setPage(swtichPage);
  };

  var IndicatorStyleActive = {
    background: "#c1ff00"
  };
  var IndicatorStyleNotActive = {
    background: "transparent"
  };

  return (
    <LoginFormBackground LoginFormWrapperBackground>
      <Helmet>
        <title>Prisijungimas / Registracija</title>
        <meta
          name="description"
          content="Prisijungti arba registruotis į musudarbas.lt sistema."
        />
      </Helmet>
      <LoginFormWrapper>
        <HeaderWithSwitch>
          <HeaderButtonWrapper>
            <button
              style={
                page == "login" ? { fontWeight: "bold" } : { fontWeight: "100" }
              }
              onClick={() => switchPage("login")}
            >
              Prisijungimas
            </button>
            <button
              style={
                page == "register"
                  ? { fontWeight: "bold" }
                  : { fontWeight: "100" }
              }
              onClick={() => switchPage("register")}
            >
              Registracija
            </button>
          </HeaderButtonWrapper>
          <ActivePageIndicator>
            <Indicator
              style={
                page == "login" ? IndicatorStyleActive : IndicatorStyleNotActive
              }
            ></Indicator>
            <Indicator
              style={
                page == "register"
                  ? IndicatorStyleActive
                  : IndicatorStyleNotActive
              }
            ></Indicator>
          </ActivePageIndicator>
        </HeaderWithSwitch>
        <Form>
          {" "}
          {page === "login" ? (
            <LoginForm clicked={clicked} />
          ) : (
            <RegisterForm />
          )}
        </Form>
      </LoginFormWrapper>
    </LoginFormBackground>
  );
};

export default LoginAndRegister;
