import React, { useState } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import styled from "styled-components";
import SubmitButton from "../../Components/Button";
import logo from "../../inc/icons/musudarbas3.png";
import userIcon from "../../inc/icons/user.svg";
import arrowDown from "../../inc/icons/arrowDown.svg";
import plusicon from "../../inc/icons/plusicon.svg";
import { relative } from "path";
const Nav = styled.nav`
  padding-top: 20px;
  ul {
    list-style: none;
    display: flex;
    align-items: center;
    margin: 0;
    padding: 0;
    position: relative;
  }
  ul {
    padding: 0 5%;
  }
`;
const Logo = styled.img`
  height: 100px;
  width: auto;
  cursor: pointer;
`;
const NavigationLinks = styled.li`
  display: flex;
  align-items: center;
  margin-left: 10px;
  float: right;
  cursor: pointer;
  padding: 10px 20px;
  border: 2px solid #a0c334;
  transition: 0.3s border;
  color: white;
  text-transform: uppercase;
  &:hover {
    border-bottom-color: white;
  }
  &:last-child {
    border-bottom-color: #a0c334;
  }
  a {
    color: white;
    text-decoration: none;
  }
`;
const MoveToRight = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  right: 5%;
  .createPost {
    border: 3px solid white;
    border-radius: 50px;
    position: relative;
    img {
      position: absolute;
      left: -12px;
    }
  }
`;
const Profile = styled.div`
  display: flex;
`;
const ProfileIcon = styled.div`
  height: 40px;
  width: 40px;
  display: block;
  border-radius: 50%;
  border: 4px solid white;
  img {
    height: 40px;
    width: 40px;
    display: block;
    border-radius: 50%;
  }
`;
const ProfileUser = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  div {
    margin-left: 5px;
  }
  img {
    margin-left: 5px;
  }
  p {
    margin: 0;
    text-align: center;
    &:nth-child(2) {
      font-size: 12px;
      font-weight: bold;
    }
  }
`;
const Logout = styled.div`
  background-color: white;
  position: absolute;
  z-index: 1000;
  top: 50px;
  button {
    width: 100%;
    font-size: 14px;
  }
`;
const Navigation = ({ clicked }) => {
  const [userDrop, setUserDrop] = useState(false);
  let data = JSON.parse(sessionStorage.getItem("user"));
  let [Name, ...second] = data === null ? "" : data.username.split(" ");
  let image;
  if (data === null) {
    image = userIcon;
  } else if (data.usersdetail !== null) {
    image = data.usersdetail.image.base64;
  }
  return (
    <Nav>
      <ul>
        <li>
          <Logo
            onClick={() => {
              window.location.href = "/";
            }}
            src={logo}
          ></Logo>
        </li>
        <MoveToRight>
          {data === null ? (
            ""
          ) : (
            <>
              <NavigationLinks className="createPost">
                <Link to="/panele/skelbimas?header=Sukurti darbo pasiūlymą">
                  Kurti skelbima
                </Link>
                <img src={plusicon} />
              </NavigationLinks>
            </>
          )}
          <NavigationLinks>
            <Link to="/">Darbo pasiulymai</Link>
          </NavigationLinks>
          <NavigationLinks onClick={e => console.log(e.target)}>
            <Link to="/">Apie mus</Link>
          </NavigationLinks>
          {data === null ? (
            <NavigationLinks onClick={() => clicked()}>
              Registruotis / Prisijungti
            </NavigationLinks>
          ) : (
            <>
              <NavigationLinks>
                <Link to="/panele/profilis?header=Mano Profilis">
                  Valdymo Panele
                </Link>
              </NavigationLinks>
              <NavigationLinks
                style={{ padding: 0, position: "relative" }}
                onClick={() => setUserDrop(!userDrop)}
              >
                <Profile>
                  <ProfileIcon>
                    <img src={image} />
                  </ProfileIcon>
                  <ProfileUser>
                    <div>
                      <p>{Name}</p>
                      <p>{data.role.description}</p>
                    </div>
                    <img src={arrowDown} />
                  </ProfileUser>
                </Profile>
                {userDrop ? (
                  <Logout>
                    <SubmitButton
                      text="Atsijungti"
                      navigateTo="/"
                      onClick={() => {
                        sessionStorage.removeItem("user");
                        window.location.reload();
                      }}
                    />
                  </Logout>
                ) : (
                  ""
                )}
              </NavigationLinks>
            </>
          )}
        </MoveToRight>
      </ul>
    </Nav>
  );
};

Navigation.propTypes = {};

export default Navigation;
