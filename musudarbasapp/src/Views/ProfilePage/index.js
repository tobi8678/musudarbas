import React, { useState, useEffect } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEnvelope,
  faPhone,
  faFileAlt,
  faTrashAlt,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import Button from "../../Components/Button";
import Input from "../../Components/InputWithType";
import axios from "axios";
import useForm from "react-hook-form";
import styled from "styled-components";
import avatar from "../../inc/icons/user.svg";
import FileBase64 from "react-file-base64";
import Helmet from "react-helmet";
import Modal from "../Dialog";
let Profile = JSON.parse(sessionStorage.getItem("user"));
let token = JSON.parse(sessionStorage.getItem("token"));
let profilePicture = avatar;
let usersPhone = "-";
let regexForData = /[^T]*/;

if (Profile === null) {
  profilePicture = avatar;
} else if (Profile.usersdetail !== null) {
  profilePicture = Profile.usersdetail.image.base64;
  usersPhone = Profile.usersdetail.phone;
}

const AboutProfile = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Username = styled.div`
  width: ‭min-content;
  margin-right: 3%;
  width: min-intrinsic;
  width: -webkit-min-content;
  width: -moz-min-content;
  width: min-content;
  display: table-caption;
  display: -ms-grid;
  -ms-grid-columns: min-content;
  h1 {
    text-align: center;
    text-transform: uppercase;
    font-weight: lighter;
    word-spacing: parent-width;
  }
`;
const ImageAndStatus = styled.div`
  width: ‭33.33333333333333‬;
  h3 {
    text-transform: uppercase;
    font-weight: lighter;
    text-align: center;
  }
  h2 {
    text-transform: uppercase;
    font-weight: lighter;
    text-align: center;
  }
  .profile_icon {
    display: block;
    border-radius: 50%;
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    height: 200px;
    width: 200px;
    border: 3px solid white;
    margin: 0 auto;
    position: relative;
    box-shadow: 0px 10px 12px -20px rgba(0, 0, 0, 1);
    input {
      position: absolute;
      opacity: 1;
      display: inline-block;
      padding: 200px 0 0 0;
      overflow: hidden;
      box-sizing: border-box;
      height: 100%;
      width: 100%;
      top: 0;
      left: 0;
      opacity: 0;
      z-index: 1;
      color: transparent;
      background-color: white;
      border: none;
      cursor: pointer;
      transition: opacity 0.6s;
      :hover {
        opacity: 0.7;
      }
    }
    &-hover {
      height: 100%;
      width: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      text-align: center;
      color: transparent;
      transition: background-color 0.5s;
      :hover {
        color: black;
        background-color: rgba(255, 255, 255, 0.7);
        cursor: pointer;
      }
      p {
        pointer-events: none;
      }
    }
  }
`;
const UserContacts = styled.div`
  margin-left: 3%;
  width: ‭33.33333333333333‬;
  .displayInline {
    display: flex;
    align-items: center;
    h3 {
      font-weight: lighter;
      font-size: 30px;
      margin-left: 2%;
      margin-top: 15px;
      margin-bottom: 15px;
    }
    svg {
      height: 30px;
      width: 30px;
    }
  }
`;
const Cvbank = styled.div`
  form {
    display: flex;
    align-items: center;
    width: auto;
    justify-content: flex-end;
    input {
      border: 1px solid black;
    }
    svg {
      padding-left: 3%;
      height: 40px;
      width: 40px !important;
      cursor: pointer;
    }
    #file {
      display: none;
    }
    label {
      margin: 0 2%;
      display: flex;
      justify-content: center;
      svg {
        transition: color 0.5s;
        :hover {
          color: #a0c334;
        }
      }
    }
  }
  .CVbank_header {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .CV {
    padding: 1% 3%;
    margin: 1% 0;
    display: flex;
    justify-content: space-between;
    border: 1px solid black;
    border-radius: 20px;
    transition: padding 0.5s;
    p {
      font-size: 20px;
      margin-left: 3%;
      transition: font-size 0.5s;
    }
    svg {
      color: salmon;
      height: 30px;
      width: 30px;
      transition: height 0.5s;
      transition: width 0.5s;
    }
    &_icon {
      display: flex;
      align-items: center;
      width: 100%;
    }
    :hover {
      cursor: pointer;
      padding: 1.5% 3%;
      svg {
        height: 40px;
        width: 40px;
      }
      p {
        font-size: 25px;
      }
    }
  }
`;
console.log(Profile);

const ProfilePage = () => {
  const getFiles = files => {
    setFile(files);

    axios
      .post(
        "http://185.34.52.143:1337/usersdetails",
        { image: files, user: Profile.id },
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      )
      .then(response => {
        // Handle success.
        console.log("Data: ", response.data);
      });
    console.log(file);
  };
  const fetchData = async () => {
    const result = await axios(
      "http://185.34.52.143:1337/cvs?_sort=created_at:desc&user.id=" +
        Profile.id,

      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    );
    setCvResults(result.data);
  };
  useEffect(() => {
    fetchData();
  }, []);

  const [file, setFile] = useState(false);
  const [cv, setCv] = useState(false);
  const [filename, setFilename] = useState("");
  const [cvresult, setCvResults] = useState(false);

  const [open, setOpen] = useState(false);
  const [header, setHeader] = useState("");
  const [message, setMessage] = useState("");

  const { register, handleSubmit, watch, errors } = useForm();

  const getBase64 = (file, cb) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function() {
      cb(reader.result);
    };
  };
  const onSubmit = data => {
    console.log(data.file[0]);
    getBase64(data.file[0], result => {
      axios
        .post(
          "http://185.34.52.143:1337/cvs",
          { name: data.name, file: result, user: Profile.id },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        .then(result => {
          setHeader("Dokumentas įkeltas");
          setMessage("Jūs sėkmingai įkėlėte dokumentą!");
          setOpen(true);
        });
    });
  };

  const deleteCv = id => {
    axios
      .delete("http://185.34.52.143:1337/cvs/" + id, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(() => {
        setHeader("Dokumentas ištrintas");
        setMessage("Jūs sėkmingai ištrynėte dokumentą!");
        setOpen(true);
      });
  };

  return (
    <div>
      <AboutProfile>
        <Helmet>
          <title>Mano Paskyra</title>
          <meta
            name="description"
            content="Panelė - čia galima pakeisti savo profilio nuotrauką, bei savo kontaktus. Taip pat čia galite rasite savo CV banką kurį galite pripildyti įkeldami savo CV failą."
          />
        </Helmet>
        <Username>
          <h1>{Profile.username}</h1>
        </Username>
        <ImageAndStatus>
          <label
            className="profile_icon"
            style={{
              backgroundImage: `url(${!file ? profilePicture : file.base64})`
            }}
            for="fileTrigger"
          >
            <div className="profile_icon-hover">
              <p>Keisti Profilio Nuotrauka</p>
            </div>
            <FileBase64
              multiple={false}
              onDone={getFiles.bind(this)}
              id="fileTrigger"
            />
          </label>

          <div>
            <h3>Narys nuo: {Profile.created_at.match(regexForData)}</h3>
            <h2>{Profile.role.description}</h2>
          </div>
        </ImageAndStatus>
        <UserContacts>
          <div className="displayInline">
            <FontAwesomeIcon icon={faEnvelope} /> <h3>{Profile.email}</h3>
          </div>
          <div className="displayInline">
            <FontAwesomeIcon icon={faPhone} /> <h3>+3700000000</h3>
          </div>
        </UserContacts>
      </AboutProfile>
      <Cvbank>
        <div className="CVbank_header">
          <h1>Mano CV bankas</h1>
          <div>
            {!cv ? (
              <Button
                text="Ikelti CV"
                backgroundColor="#A0C334"
                color="white"
                onClick={() => {
                  setCv(!cv);
                }}
              />
            ) : (
              <form onSubmit={handleSubmit(onSubmit)}>
                <Input
                  type="text"
                  name="name"
                  placeholder="CV pavadinimas"
                  icon={faFileAlt}
                  refFunc={register}
                />
                <label for="file">
                  <FontAwesomeIcon icon={faFileAlt} />
                  {filename}
                </label>
                <input
                  type="file"
                  id="file"
                  name="file"
                  onChange={e => {
                    setFilename(e.target.value);
                  }}
                  ref={register}
                />

                <Button
                  text="Įkelti"
                  backgroundColor="#A0C334"
                  color="white"
                  onClick={() => {
                    setCv(!cv);
                  }}
                />
                <FontAwesomeIcon icon={faTimes} onClick={() => setCv(!cv)} />
              </form>
            )}
          </div>
        </div>
        <hr></hr>

        {!cvresult ? (
          <></>
        ) : (
          <>
            {cvresult.length <= 0 ? (
              <h1>Just neturite nei vieno CV</h1>
            ) : (
              cvresult.map(onecv => (
                <a
                  style={{ color: "black" }}
                  download={onecv.name}
                  href={onecv.file}
                >
                  <div className="CV">
                    <div className="CV_icon">
                      <FontAwesomeIcon icon={faFileAlt} />
                      <p>{onecv.name}</p>
                    </div>
                    <div className="CV_actions">
                      <FontAwesomeIcon
                        icon={faTrashAlt}
                        onClick={e => deleteCv(onecv.id)}
                      />
                    </div>
                  </div>
                </a>
              ))
            )}
          </>
        )}
      </Cvbank>
      <Modal
        open={open}
        func={() => {
          window.location.reload();
        }}
        header={header}
        text={message}
      />
    </div>
  );
};

export default ProfilePage;
