import React, { useState, useEffect } from "react";
import Detailed from "../ControlJobPost/Detailed";
import axios from "axios";
import Helmet from "react-helmet";

const ControlJobPost = () => {
  const [posts, setPosts] = useState("");
  const fetchData = async () => {
    const result = await axios(
      "http://185.34.52.143:1337/jobposts?confirmed=false"
    );
    setPosts(result.data);
  };
  useEffect(() => {
    fetchData();
  }, []);
  let [, setState] = useState();
  return (
    <div>
      <Helmet>
        <title>Darbo Skelbimų Administravimas</title>
        <meta
          name="description"
          content="Darbo Skelbimų administravimas. Vartotojai turintys administratoriaus statusą gali peržiųrėti anketas, jas redaguoti, patvirtinti arba jas ištrinti"
        />
      </Helmet>
      {posts !== "" ? (
        posts.map((posts, index) => <Detailed key={index} data={posts} />)
      ) : (
        <></>
      )}
    </div>
  );
};

export default ControlJobPost;
