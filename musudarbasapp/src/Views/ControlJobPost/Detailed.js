import React, { useState } from "react";
import styled from "styled-components";
import logo from "../../inc/icons/musudarbas3.png";
import close from "../../inc/icons/x.svg";
import open from "../../inc/icons/open.svg";
import Button from "../../Components/Button";
import axios from "axios";

const Logo = styled.div`
  height: 200px;
  width: 200px;
  background-image: url(${logo});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
`;
const Description = styled.div`
  margin-left: 8%;

  h3 {
    text-transform: uppercase;
    span {
      font-weight: lighter;
    }
  }
  p {
  }
`;
const Controls = styled.div`
  display: flex;
  justify-content: center;
  padding-bottom: 15px;
  box-shadow: 0px 0px 110px -74px rgba(0, 0, 0, 0.75);
  button {
    margin-left: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
`;
const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 0px 0px 110px -74px rgba(0, 0, 0, 0.75);
  border-radius: 20px;
  position: relative;
  img {
    cursor: pointer;
    height: 42px;
    width: 42px;
    position: absolute;
    top: 20px;
    right: 20px;
  }
`;
const ConfirmAction = styled.div`
  padding-bottom: 15px;
  .Button_Wrapper {
    display: flex;
    justify-content: center;
  }
  button {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    width: 90%;
  }
`;
const Detailed = ({ data }) => {
  const [control, setControl] = useState(false);
  const [confirm, setConfirm] = useState([false, "confirm"]);
  const [deleted, setDelated] = useState(false);
  let token = JSON.parse(sessionStorage.getItem("token"));
  console.log(data);

  const confirmPost = () => {
    if (confirm[1] === "confirm") {
      axios
        .put(
          "http://185.34.52.143:1337/jobposts/" + data.id,
          { confirmed: true },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        .then(response => {
          // Handle success.
          console.log("Updated");
          setDelated(true);
        })
        .catch(error => {
          // Handle error.
          console.log("An error occurred:", error);
        });
    } else {
      axios
        .delete("http://185.34.52.143:1337/jobposts/" + data.id, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(response => {
          // Handle success.
          console.log("Updated");
          setDelated(true);
        })
        .catch(error => {
          // Handle error.
          console.log("An error occurred:", error);
        });
    }
  };
  return (
    <div style={{ display: !deleted ? "block" : "none" }}>
      <Wrapper>
        <Logo></Logo>
        <Description>
          <h3>Pavadinimas:</h3>
          <p>{data.name}</p>
          <h3>APRAŠYMAS:</h3>
          <p>{data.description}</p>
          <h3>
            SUKURTA VARTOTOJO: <span>{data.user.username}</span>
          </h3>
        </Description>
        <img
          alt="Mygtukas gryžti atgal"
          src={control || confirm[0] ? close : open}
          onClick={() => {
            setControl(!control);
            setConfirm([false, "confirm"]);
          }}
        />
      </Wrapper>
      {control ? (
        <Controls>
          <Button
            text="Patvirtinti"
            color="white"
            backgroundColor="#79CF18"
            onClick={() => {
              setControl(!control);
              setConfirm([!confirm[0], "confirm"]);
            }}
          />
          <Button
            text="Peržiūrėti"
            color="white"
            backgroundColor="#3892E3"
            navigateTo={"/skelbimas/" + data.id}
          />
          <Button
            text="Redaguoti"
            color="white"
            backgroundColor="#33E0C2"
            onClick={() =>
              window.open(
                `http://185.34.52.143:1337/admin/plugins/content-manager/jobpost/${data.id}?redirectUrl=/plugins/content-manager/jobpost?source=content-manager`,
                "_blank"
              )
            }
          />
          <Button
            text="Ištrinti"
            color="white"
            backgroundColor="#D2000C"
            onClick={() => {
              setControl(!control);
              setConfirm([!confirm[0], "delete"]);
            }}
          />
        </Controls>
      ) : (
        <></>
      )}
      {confirm[0] ? (
        <ConfirmAction>
          <Button
            text={
              confirm[1] === "confirm"
                ? "PATVIRTINTI PATVIRTINIMA"
                : "PATVIRTINTI IŠTRINIMA"
            }
            color="white"
            backgroundColor={confirm[1] === "confirm" ? "#79CF18" : "#D2000C"}
            onClick={confirmPost}
          />
        </ConfirmAction>
      ) : (
        <></>
      )}
    </div>
  );
};

export default Detailed;
