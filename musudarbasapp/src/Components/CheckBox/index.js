import React, { useState } from "react";
import styled from "styled-components";
import checkIcon from "../../inc/icons/chek.svg";

const Input = styled.input`
  display: none;
`;
const Label = styled.label`
  display: block;
  position: relative;
  width: 43px;
  height: 40px;
  border-radius: 10px;
  cursor: pointer;
  user-select: none;
  background: white;
  border: 1px transparent solid;
  box-shadow: 0px 0px 110px -74px rgba(0, 0, 0, 0.75);
`;
const CheckImage = styled.img`
  position: absolute;
  height: 30px;
  width: 33px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const CheckBoxComponentWrapper = styled.div`
  display: flex;
  align-items: center;
`;
const CheckBox = ({ onClick }) => {
  const [check, setCheck] = useState(false);
  return (
    <CheckBoxComponentWrapper>
      <Label
        onClick={() => {
          setCheck(!check);
          if (onClick === undefined) {
            console.log("no function");
          } else onClick();
        }}
        for="forButton"
      >
        {check && <CheckImage src={checkIcon}></CheckImage>}
      </Label>
      <Input name="" type="checkbox" value={check} id="forButton" />
    </CheckBoxComponentWrapper>
  );
};
export default CheckBox;
