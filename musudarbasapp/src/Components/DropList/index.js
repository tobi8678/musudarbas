import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import arrow from "../../inc/icons/arrowBlack.svg";
const DropListWrapper = styled.div`
  position: relative;
  width: 100%;
  input {
    width: 100%;

    background-repeat: no-repeat;
    background-position: 98% center;
    margin-bottom: 0px !important;
  }
`;
const DropSelection = styled.div`
  overflow-y: scroll;
  position: absolute;
  z-index: 1000;
  background-color: #f7f7f7;
  min-height: auto;
  max-height: 200px;
  border-radius: 5px;
  width: 100%;
  padding: 10px 20px;
  ul {
    list-style: none;
    padding: 0;
    margin: 0;
    li {
      font-size: 20px;
      color: black;
      padding: 5px;
      &:hover {
        cursor: pointer;
        background-color: #c4c4c4;
      }
    }
  }
`;
const Error = styled.p`
  color: salmon;
  padding-top: 5px;
`;
const DropList = ({
  array,
  read,
  placeholder,
  name,
  refFunc,
  warning,
  req,
  id,
  icon,
  onClick,
  background
}) => {
  const onScroll = e => {
    const bottom =
      e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
    if (bottom) {
      setScroll(scroll + 7);
      setSlicearray(array.slice(0, scroll));
    } else setScroll(scroll);
  };
  const [active, setActive] = useState(false);
  const [value, setValue] = useState("");
  const [typedValue, settypedValue] = useState("");
  const [clicked, setClicked] = useState(false);
  const [scroll, setScroll] = useState(6);
  const [slicearray, setSlicearray] = useState(array.slice(0, 6));
  const [error, setError] = useState("");
  const checkError = e => {
    let element = e.target.value;
    for (let i = 0; i <= array.length; i++) {
      if (element === array[i]) {
        console.log([value, array[i]]);
        setError("");
        break;
      } else setError(warning);
    }
  };
  const handleOnClick = e => {
    let elementValue = e.target.getAttribute("select-value");
    setValue(elementValue);
    settypedValue(elementValue);
  };
  const typeValue = e => {
    let arrayOfStoredCities = [];
    for (let i = 0; i <= array.length; i++) {
      if (array[i] !== undefined && array[i].includes(typedValue)) {
        arrayOfStoredCities.push(array[i]);
      }
    }
    setSlicearray(arrayOfStoredCities);
    if (!clicked) {
      let typedValue = e.target.value;
      settypedValue(typedValue);
    } else {
      setClicked(false);
      settypedValue(value);
    }
  };
  console.log(background);
  return (
    <DropListWrapper className="Drop_List" id={id}>
      <input
        autoComplete="off"
        name={name}
        ref={refFunc}
        type="text"
        placeholder={placeholder}
        onFocus={() => setActive(!active)}
        onBlur={e => {
          setTimeout(function() {
            setActive(!active);
          }, 400);
          checkError(e);
        }}
        onChange={e => {
          typeValue(e);
          checkError(e);
        }}
        value={clicked ? value : typedValue}
        readOnly={read ? "readonly" : ""}
        required={req ? true : false}
        style={{
          backgroundImage: `url(${icon ? icon : arrow})`,
          backgroundColor: background ? background : "",
          placeholder: background ? "white" : ""
        }}
      />

      {active ? (
        <DropSelection
          id="style-1"
          className="Drop_Down"
          onScroll={e => onScroll(e)}
        >
          <ul id="DropAmount">
            {slicearray.map(city => (
              <li
                select-value={city}
                onClick={e => {
                  handleOnClick(e);
                  if (onClick) {
                    onClick(e.target.getAttribute("select-value"));
                  }
                }}
              >
                {city}
              </li>
            ))}
          </ul>
        </DropSelection>
      ) : (
        ""
      )}
    </DropListWrapper>
  );
};

DropList.propTypes = {};

export default DropList;
