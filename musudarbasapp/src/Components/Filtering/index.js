import React, {useState} from 'react';
import styled from "styled-components";
import PropTypes from "prop-types";
import jobIcon from "../../inc/icons/jobicon.svg";
import arrow from "../../inc/icons/arrowDown.svg";
import btn from "../../inc/icons/avatar.png";


const Filtering = ({title}) => {

  const [state, setState] = useState({
    job: "",
    location: "",
    distance: "",
    select: [],
    errmsg: "" 
  });

  const handleChange = (e) => {    
    const value = e.target.value;    
    setState({
      ...state,
      [e.target.name]: value
    });
  }

  const onSubmit = (e) => {
    e.preventDefault();
  }
 
  return ( 
    <FormWrapper>
       <Title>{title}</Title>
      <Form  noValidate>
        <FormGroup>
          <Input  type="text" name="job" value={state.job} onChange={handleChange} required/>
          <Select icon={arrow} name="select" onChange={handleChange} value={state.select}>
              <Option>
                {title}
              </Option>
              <Option>
                {title}
              </Option>
              <Option>
                {title}
              </Option>
              <Option>
                {title}
              </Option>
          </Select>
        </FormGroup>
        <FormGroup>
          <Input type="text" icon={jobIcon} name="location" value={state.location} onChange={handleChange} required/>
        </FormGroup>
        <FormGroup>
          <Input  type="number" name="distance" value={state.distance} onChange={handleChange} required/>
        </FormGroup>
        
        <Button type="submit" icon={btn} name="submit"  onSubmit={onSubmit}><img src={btn}/></Button>
        
      </Form>
    </ FormWrapper>
  );
}

Filtering.propTypes = {
  title: PropTypes.string.isRequired,
  locationValue: PropTypes.string.isRequired,
};


const Title = styled.h1`
  text-align:center;
  font-size:6rem;
  padding:16px 0px;
`;

const Select = styled.select`
  display:block;
  width:150px;
  text-align:center;
  font-size:30px;
  font-weight:300;
  max-width: 100%; 
  box-sizing: border-box;
  background:url(${props => props.icon ? props.icon : "none"}) no-repeat;
  background-position-y:center;
  background-position-x:95%;
  color:#FFF;
  border:none;
  border-left:1px solid #FFF;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  font-family:"Montserrat", sans-serif;
`;

const Option = styled.option`
  background:none;
`;

const Form  = styled.form`
  display:flex;
  flex:1;
  flex-wrap:wrap;
  justify-content:center;
  padding-bottom:100px;
`;

const FormWrapper = styled.div`
    background:#81A21B;
    color:#FFF;
    padding:32px;
`;


const FormGroup = styled.div`
  display:flex;
  margin:18px 14px;
  border-radius:14px;
  background:#A0C334;

`;

const Button = styled.button` 
   background:none;
   border:none;
`;

const Input = styled.input`
  width: calc(70%);
  flex:1;
  color: #FFF;
  font-size: 24px;
  background:url( ${props => props.icon ? props.icon : "none"}) no-repeat right;
  background-position-x:95%;
  border:none;
  padding:33px;
  font-family:"Montserrat", sans-serif;
`;

export default Filtering;
