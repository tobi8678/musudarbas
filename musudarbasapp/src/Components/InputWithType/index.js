import React from "react";
import styled from "styled-components";

const InputWithType = ({
  type,
  name,
  placeholder,
  icon,
  refFunc,
  value,
  onChange
}) => {
  return (
    <>
      <Input
        type={type}
        name={name}
        placeholder={placeholder}
        icon={icon}
        ref={refFunc}
        value={value}
        onChange={onChange}
      />
    </>
  );
};

const Input = styled.input`
  color: #5d5d5d;
  border-radius: 32px;
  font-size: 24px;
  padding: 20px 20px;
  padding-left: 60px;
  background: url(${props => props.icon}) no-repeat scroll 20px 20px;
  background-size: 30px;
  border: none;
  background-color: white;
  box-sizing: border-box;
  border: 1px transparent solid;
  box-shadow: 0px 0px 110px -74px rgba(0, 0, 0, 0.75);
`;
export default InputWithType;
